#include<stdio.h>
#include "ctest.h"

int length(list hi){
    if_cons(hi, hi_head, hi_tail)
        return 1 + length(hi_tail);
    else()
        return 0;
    end_if()
}

void main(){
    list_char char_list = cons_list_char('a', cons_list_char('b', cons_list_char('c', nil_list_char())));
    list_int int_list = cons_list_int(1, cons_list_int(2, cons_list_int(3, nil_list_int())));
    list_list_char list_char_list = cons_list_list_char(char_list, nil_list_list_char());
    
    int char_int;
    int int_int;
    int list_char_int;
    
    char_int = length_char(char_list);
    int_int = length_int(int_list);
    list_char_int = length_list_char(list_char_list);
    
    printf("Integer List: %i\nChar List: %i\nList Char List: %i\n", int_int, char_int, list_char_int);
    
    exit(0);
}