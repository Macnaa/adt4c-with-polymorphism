#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_pair_t{} *pair_t;
#define pair_t_T_NUM_CONST (0) 
#define pair_t_T_NUM_NONCONST (1) 
#define pair_t_constructorNum(v) \
(( pair_t_T_NUM_NONCONST>0 && v >= pair_t_T_NUM_CONST ?  \
 ( pair_t_T_NUM_NONCONST==1 ?  pair_t_T_NUM_CONST   : \
(( pair_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + pair_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + pair_t_T_NUM_CONST)) : v ) )

static __inline void free_pair_t(pair_t v);
#define switch_pair_t(v) \
{pair_t _pair_t_tchk, _ADT_v=(v); \
int t = pair_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_pair_t(pair_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_pair_t_pair {
    ADT_1 f0;
    ADT_2 f1;};
#define if_pair(v, v0, v1) \
{pair_t _ADT_v=(v);if (1) {\
ADT_1 v0=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 v1=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair(v0, v1) \
} else if (1) {\
ADT_1 v0=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 v1=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_ptr(v, v0, v1) \
{pair_t _ADT_v=(v);if (1) {\
ADT_1 *v0=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 *v1=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair(v0, v1) \
break;}} case 0: {{pair_t _SW_tchk=_pair_t_tchk;}{char _pair_t_tchk; \
ADT_1 v0=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 v1=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_ptr(v0, v1) \
break;}} case 0: {{pair_t _SW_tchk=_pair_t_tchk;}{char _pair_t_tchk; \
ADT_1 *v0=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 *v1=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 

static __inline pair_t pair(ADT_1 v0, ADT_2 v1){
	struct _ADT_pair_t_pair *v=(struct _ADT_pair_t_pair*)ADT_MALLOC(sizeof(struct _ADT_pair_t_pair));
	v->f0=v0; 
	v->f1=v1; 
	return (pair_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_int_char{} *int_char;
#define int_char_T_NUM_CONST (0) 
#define int_char_T_NUM_NONCONST (1) 
#define int_char_constructorNum(v) \
(( int_char_T_NUM_NONCONST>0 && v >= int_char_T_NUM_CONST ?  \
 ( int_char_T_NUM_NONCONST==1 ?  int_char_T_NUM_CONST   : \
(( int_char_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + int_char_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + int_char_T_NUM_CONST)) : v ) )

static __inline void free_int_char(int_char v);
#define switch_int_char(v) \
{int_char _int_char_tchk, _ADT_v=(v); \
int t = int_char_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_int_char(int_char v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_int_char_pair_int_char {
    int f0;
    char f1;};
#define if_pair_int_char(v, v0, v1) \
{int_char _ADT_v=(v);if (1) {\
int v0=((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f0; \
char v1=((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair_int_char(v0, v1) \
} else if (1) {\
int v0=((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f0; \
char v1=((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_int_char_ptr(v, v0, v1) \
{int_char _ADT_v=(v);if (1) {\
int *v0=&((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f0; \
char *v1=&((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_int_char(v0, v1) \
break;}} case 0: {{int_char _SW_tchk=_int_char_tchk;}{char _int_char_tchk; \
int v0=((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f0; \
char v1=((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_int_char_ptr(v0, v1) \
break;}} case 0: {{int_char _SW_tchk=_int_char_tchk;}{char _int_char_tchk; \
int *v0=&((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f0; \
char *v1=&((struct _ADT_int_char_pair_int_char*)((uintptr_t)_ADT_v-0))->f1; 

static __inline int_char pair_int_char(int v0, char v1){
	struct _ADT_int_char_pair_int_char *v=(struct _ADT_int_char_pair_int_char*)ADT_MALLOC(sizeof(struct _ADT_int_char_pair_int_char));
	v->f0=v0; 
	v->f1=v1; 
	return (int_char)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_char_int{} *char_int;
#define char_int_T_NUM_CONST (0) 
#define char_int_T_NUM_NONCONST (1) 
#define char_int_constructorNum(v) \
(( char_int_T_NUM_NONCONST>0 && v >= char_int_T_NUM_CONST ?  \
 ( char_int_T_NUM_NONCONST==1 ?  char_int_T_NUM_CONST   : \
(( char_int_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + char_int_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + char_int_T_NUM_CONST)) : v ) )

static __inline void free_char_int(char_int v);
#define switch_char_int(v) \
{char_int _char_int_tchk, _ADT_v=(v); \
int t = char_int_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_char_int(char_int v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_char_int_pair_char_int {
    char f0;
    int f1;};
#define if_pair_char_int(v, v0, v1) \
{char_int _ADT_v=(v);if (1) {\
char v0=((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f0; \
int v1=((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair_char_int(v0, v1) \
} else if (1) {\
char v0=((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f0; \
int v1=((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_char_int_ptr(v, v0, v1) \
{char_int _ADT_v=(v);if (1) {\
char *v0=&((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f0; \
int *v1=&((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_char_int(v0, v1) \
break;}} case 0: {{char_int _SW_tchk=_char_int_tchk;}{char _char_int_tchk; \
char v0=((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f0; \
int v1=((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_char_int_ptr(v0, v1) \
break;}} case 0: {{char_int _SW_tchk=_char_int_tchk;}{char _char_int_tchk; \
char *v0=&((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f0; \
int *v1=&((struct _ADT_char_int_pair_char_int*)((uintptr_t)_ADT_v-0))->f1; 

static __inline char_int pair_char_int(char v0, int v1){
	struct _ADT_char_int_pair_char_int *v=(struct _ADT_char_int_pair_char_int*)ADT_MALLOC(sizeof(struct _ADT_char_int_pair_char_int));
	v->f0=v0; 
	v->f1=v1; 
	return (char_int)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_swapped{} *swapped;
#define swapped_T_NUM_CONST (0) 
#define swapped_T_NUM_NONCONST (1) 
#define swapped_constructorNum(v) \
(( swapped_T_NUM_NONCONST>0 && v >= swapped_T_NUM_CONST ?  \
 ( swapped_T_NUM_NONCONST==1 ?  swapped_T_NUM_CONST   : \
(( swapped_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + swapped_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + swapped_T_NUM_CONST)) : v ) )

static __inline void free_swapped(swapped v);
#define switch_swapped(v) \
{swapped _swapped_tchk, _ADT_v=(v); \
int t = swapped_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_swapped(swapped v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_swapped_pair_swapped {
    ADT_2 f0;
    ADT_1 f1;};
#define if_pair_swapped(v, v0, v1) \
{swapped _ADT_v=(v);if (1) {\
ADT_2 v0=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 v1=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair_swapped(v0, v1) \
} else if (1) {\
ADT_2 v0=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 v1=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_swapped_ptr(v, v0, v1) \
{swapped _ADT_v=(v);if (1) {\
ADT_2 *v0=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 *v1=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_swapped(v0, v1) \
break;}} case 0: {{swapped _SW_tchk=_swapped_tchk;}{char _swapped_tchk; \
ADT_2 v0=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 v1=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_swapped_ptr(v0, v1) \
break;}} case 0: {{swapped _SW_tchk=_swapped_tchk;}{char _swapped_tchk; \
ADT_2 *v0=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 *v1=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 

static __inline swapped pair_swapped(ADT_2 v0, ADT_1 v1){
	struct _ADT_swapped_pair_swapped *v=(struct _ADT_swapped_pair_swapped*)ADT_MALLOC(sizeof(struct _ADT_swapped_pair_swapped));
	v->f0=v0; 
	v->f1=v1; 
	return (swapped)(0+(uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/

swapped swap_pair(pair_t);

/*----------------------------------------------------------------------------*/

/* Function Aliases */
/*----------------------------------------------------------------------------*/

static __inline int_char swap_char_int(char_int v0){
    return (int_char) swap_pair((pair_t) v0);
}


/*----------------------------------------------------------------------------*/
