#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_decl_t{} *decl_t;
#define decl_t_T_NUM_CONST (0) 
#define decl_t_T_NUM_NONCONST (2) 
#define decl_t_constructorNum(v) \
(( decl_t_T_NUM_NONCONST>0 && v >= decl_t_T_NUM_CONST ?  \
 ( decl_t_T_NUM_NONCONST==1 ?  decl_t_T_NUM_CONST   : \
(( decl_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + decl_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + decl_t_T_NUM_CONST)) : v ) )

static __inline void free_decl_t(decl_t v);
#define switch_decl_t(v) \
{decl_t _decl_t_tchk, _ADT_v=(v); \
int t = decl_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_decl_t(decl_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v&~LOW_BIT_USED));
	}
}

;
struct _ADT_decl_t_paramdecl {
    struct _ADT_string_t* f0;
    struct _ADT_ctrlist_t* f1;
    struct _ADT_paramlist_t* f2;};
#define if_paramdecl(v, v0, v1, v2) \
{decl_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0) {\
struct _ADT_string_t* v0=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* v2=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f2; 
#define else_if_paramdecl(v0, v1, v2) \
} else if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
struct _ADT_string_t* v0=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* v2=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f2; 
#define if_paramdecl_ptr(v, v0, v1, v2) \
{decl_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
struct _ADT_string_t* *v0=&((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* *v1=&((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* *v2=&((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f2; 
#define case_paramdecl(v0, v1, v2) \
break;}} case 0: {{decl_t _SW_tchk=_decl_t_tchk;}{char _decl_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* v2=((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f2; 
#define case_paramdecl_ptr(v0, v1, v2) \
break;}} case 0: {{decl_t _SW_tchk=_decl_t_tchk;}{char _decl_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* *v1=&((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* *v2=&((struct _ADT_decl_t_paramdecl*)((uintptr_t)_ADT_v-0))->f2; 

static __inline decl_t paramdecl(struct _ADT_string_t* v0, struct _ADT_ctrlist_t* v1, struct _ADT_paramlist_t* v2){
	struct _ADT_decl_t_paramdecl *v=(struct _ADT_decl_t_paramdecl*)ADT_MALLOC(sizeof(struct _ADT_decl_t_paramdecl));
	v->f0=v0; 
	v->f1=v1; 
	v->f2=v2; 
	return (decl_t)(0+(uintptr_t)v);
}
;
struct _ADT_decl_t_decl {
    struct _ADT_string_t* f0;
    struct _ADT_ctrlist_t* f1;};
#define if_decl(v, v0, v1) \
{decl_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1) {\
struct _ADT_string_t* v0=((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f1; 
#define else_if_decl(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
struct _ADT_string_t* v0=((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f1; 
#define if_decl_ptr(v, v0, v1) \
{decl_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
struct _ADT_string_t* *v0=&((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_ctrlist_t* *v1=&((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f1; 
#define case_decl(v0, v1) \
break;}} case 1: {{decl_t _SW_tchk=_decl_t_tchk;}{char _decl_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f1; 
#define case_decl_ptr(v0, v1) \
break;}} case 1: {{decl_t _SW_tchk=_decl_t_tchk;}{char _decl_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_ctrlist_t* *v1=&((struct _ADT_decl_t_decl*)((uintptr_t)_ADT_v-1))->f1; 

static __inline decl_t decl(struct _ADT_string_t* v0, struct _ADT_ctrlist_t* v1){
	struct _ADT_decl_t_decl *v=(struct _ADT_decl_t_decl*)ADT_MALLOC(sizeof(struct _ADT_decl_t_decl));
	v->f0=v0; 
	v->f1=v1; 
	return (decl_t)(1+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_ctr_t{} *ctr_t;
#define ctr_t_T_NUM_CONST (0) 
#define ctr_t_T_NUM_NONCONST (1) 
#define ctr_t_constructorNum(v) \
(( ctr_t_T_NUM_NONCONST>0 && v >= ctr_t_T_NUM_CONST ?  \
 ( ctr_t_T_NUM_NONCONST==1 ?  ctr_t_T_NUM_CONST   : \
(( ctr_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + ctr_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + ctr_t_T_NUM_CONST)) : v ) )

static __inline void free_ctr_t(ctr_t v);
#define switch_ctr_t(v) \
{ctr_t _ctr_t_tchk, _ADT_v=(v); \
int t = ctr_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_ctr_t(ctr_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_ctr_t_ctr {
    struct _ADT_string_t* f0;
    struct _ADT_arglist_t* f1;};
#define if_ctr(v, v0, v1) \
{ctr_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* v0=((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_ctr(v0, v1) \
} else if (1) {\
struct _ADT_string_t* v0=((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f1; 
#define if_ctr_ptr(v, v0, v1) \
{ctr_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* *v0=&((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f1; 
#define case_ctr(v0, v1) \
break;}} case 0: {{ctr_t _SW_tchk=_ctr_t_tchk;}{char _ctr_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f1; 
#define case_ctr_ptr(v0, v1) \
break;}} case 0: {{ctr_t _SW_tchk=_ctr_t_tchk;}{char _ctr_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_ctr_t_ctr*)((uintptr_t)_ADT_v-0))->f1; 

static __inline ctr_t ctr(struct _ADT_string_t* v0, struct _ADT_arglist_t* v1){
	struct _ADT_ctr_t_ctr *v=(struct _ADT_ctr_t_ctr*)ADT_MALLOC(sizeof(struct _ADT_ctr_t_ctr));
	v->f0=v0; 
	v->f1=v1; 
	return (ctr_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_arg_t{} *arg_t;
#define arg_t_T_NUM_CONST (0) 
#define arg_t_T_NUM_NONCONST (4) 
#define arg_t_constructorNum(v) \
(( arg_t_T_NUM_NONCONST>0 && v >= arg_t_T_NUM_CONST ?  \
 ( arg_t_T_NUM_NONCONST==1 ?  arg_t_T_NUM_CONST   : \
(( arg_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + arg_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + arg_t_T_NUM_CONST)) : v ) )

static __inline void free_arg_t(arg_t v);
#define switch_arg_t(v) \
{arg_t _arg_t_tchk, _ADT_v=(v); \
int t = arg_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_arg_t(arg_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v&~LOW_BIT_USED));
	}
}

;
struct _ADT_arg_t_argfunct {
    struct _ADT_string_t* f0;
    struct _ADT_arglist_t* f1;};
#define if_argfunct(v, v0, v1) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0) {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_argfunct(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f1; 
#define if_argfunct_ptr(v, v0, v1) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f1; 
#define case_argfunct(v0, v1) \
break;}} case 0: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f1; 
#define case_argfunct_ptr(v0, v1) \
break;}} case 0: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_arg_t_argfunct*)((uintptr_t)_ADT_v-0))->f1; 

static __inline arg_t argfunct(struct _ADT_string_t* v0, struct _ADT_arglist_t* v1){
	struct _ADT_arg_t_argfunct *v=(struct _ADT_arg_t_argfunct*)ADT_MALLOC(sizeof(struct _ADT_arg_t_argfunct));
	v->f0=v0; 
	v->f1=v1; 
	return (arg_t)(0+(uintptr_t)v);
}
;
struct _ADT_arg_t_paramdata {
    struct _ADT_string_t* f0;
    struct _ADT_paramlist_t* f1;};
#define if_paramdata(v, v0, v1) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1) {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_paramlist_t* v1=((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f1; 
#define else_if_paramdata(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_paramlist_t* v1=((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f1; 
#define if_paramdata_ptr(v, v0, v1) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_paramlist_t* *v1=&((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f1; 
#define case_paramdata(v0, v1) \
break;}} case 1: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_paramlist_t* v1=((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f1; 
#define case_paramdata_ptr(v0, v1) \
break;}} case 1: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f0; \
struct _ADT_paramlist_t* *v1=&((struct _ADT_arg_t_paramdata*)((uintptr_t)_ADT_v-1))->f1; 

static __inline arg_t paramdata(struct _ADT_string_t* v0, struct _ADT_paramlist_t* v1){
	struct _ADT_arg_t_paramdata *v=(struct _ADT_arg_t_paramdata*)ADT_MALLOC(sizeof(struct _ADT_arg_t_paramdata));
	v->f0=v0; 
	v->f1=v1; 
	return (arg_t)(1+(uintptr_t)v);
}
;
struct _ADT_arg_t_paramarg {
    struct _ADT_string_t* f0;};
#define if_paramarg(v, v0) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==2) {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_paramarg*)((uintptr_t)_ADT_v-2))->f0; 
#define else_if_paramarg(v0) \
} else if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==2)  {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_paramarg*)((uintptr_t)_ADT_v-2))->f0; 
#define if_paramarg_ptr(v, v0) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==2)  {\
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_paramarg*)((uintptr_t)_ADT_v-2))->f0; 
#define case_paramarg(v0) \
break;}} case 2: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_arg_t_paramarg*)((uintptr_t)_ADT_v-2))->f0; 
#define case_paramarg_ptr(v0) \
break;}} case 2: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_paramarg*)((uintptr_t)_ADT_v-2))->f0; 

static __inline arg_t paramarg(struct _ADT_string_t* v0){
	struct _ADT_arg_t_paramarg *v=(struct _ADT_arg_t_paramarg*)ADT_MALLOC(sizeof(struct _ADT_arg_t_paramarg));
	v->f0=v0; 
	return (arg_t)(2+(uintptr_t)v);
}
;
struct _ADT_arg_t_arg {
    struct _ADT_string_t* f0;};
#define if_arg(v, v0) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==3) {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_arg*)((uintptr_t)_ADT_v-3))->f0; 
#define else_if_arg(v0) \
} else if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==3)  {\
struct _ADT_string_t* v0=((struct _ADT_arg_t_arg*)((uintptr_t)_ADT_v-3))->f0; 
#define if_arg_ptr(v, v0) \
{arg_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 0 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==3)  {\
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_arg*)((uintptr_t)_ADT_v-3))->f0; 
#define case_arg(v0) \
break;}} case 3: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_arg_t_arg*)((uintptr_t)_ADT_v-3))->f0; 
#define case_arg_ptr(v0) \
break;}} case 3: {{arg_t _SW_tchk=_arg_t_tchk;}{char _arg_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_arg_t_arg*)((uintptr_t)_ADT_v-3))->f0; 

static __inline arg_t arg(struct _ADT_string_t* v0){
	struct _ADT_arg_t_arg *v=(struct _ADT_arg_t_arg*)ADT_MALLOC(sizeof(struct _ADT_arg_t_arg));
	v->f0=v0; 
	return (arg_t)(3+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_param_t{} *param_t;
#define param_t_T_NUM_CONST (0) 
#define param_t_T_NUM_NONCONST (1) 
#define param_t_constructorNum(v) \
(( param_t_T_NUM_NONCONST>0 && v >= param_t_T_NUM_CONST ?  \
 ( param_t_T_NUM_NONCONST==1 ?  param_t_T_NUM_CONST   : \
(( param_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + param_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + param_t_T_NUM_CONST)) : v ) )

static __inline void free_param_t(param_t v);
#define switch_param_t(v) \
{param_t _param_t_tchk, _ADT_v=(v); \
int t = param_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_param_t(param_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_param_t_param {
    struct _ADT_string_t* f0;};
#define if_param(v, v0) \
{param_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* v0=((struct _ADT_param_t_param*)((uintptr_t)_ADT_v-0))->f0; 
#define else_if_param(v0) \
} else if (1) {\
struct _ADT_string_t* v0=((struct _ADT_param_t_param*)((uintptr_t)_ADT_v-0))->f0; 
#define if_param_ptr(v, v0) \
{param_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* *v0=&((struct _ADT_param_t_param*)((uintptr_t)_ADT_v-0))->f0; 
#define case_param(v0) \
break;}} case 0: {{param_t _SW_tchk=_param_t_tchk;}{char _param_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_param_t_param*)((uintptr_t)_ADT_v-0))->f0; 
#define case_param_ptr(v0) \
break;}} case 0: {{param_t _SW_tchk=_param_t_tchk;}{char _param_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_param_t_param*)((uintptr_t)_ADT_v-0))->f0; 

static __inline param_t param(struct _ADT_string_t* v0){
	struct _ADT_param_t_param *v=(struct _ADT_param_t_param*)ADT_MALLOC(sizeof(struct _ADT_param_t_param));
	v->f0=v0; 
	return (param_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_inst_t{} *inst_t;
#define inst_t_T_NUM_CONST (0) 
#define inst_t_T_NUM_NONCONST (1) 
#define inst_t_constructorNum(v) \
(( inst_t_T_NUM_NONCONST>0 && v >= inst_t_T_NUM_CONST ?  \
 ( inst_t_T_NUM_NONCONST==1 ?  inst_t_T_NUM_CONST   : \
(( inst_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + inst_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + inst_t_T_NUM_CONST)) : v ) )

static __inline void free_inst_t(inst_t v);
#define switch_inst_t(v) \
{inst_t _inst_t_tchk, _ADT_v=(v); \
int t = inst_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_inst_t(inst_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_inst_t_inst {
    struct _ADT_string_t* f0;
    struct _ADT_string_t* f1;
    struct _ADT_paramlist_t* f2;};
#define if_inst(v, v0, v1, v2) \
{inst_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* v0=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_string_t* v1=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* v2=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f2; 
#define else_if_inst(v0, v1, v2) \
} else if (1) {\
struct _ADT_string_t* v0=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_string_t* v1=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* v2=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f2; 
#define if_inst_ptr(v, v0, v1, v2) \
{inst_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* *v0=&((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_string_t* *v1=&((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* *v2=&((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f2; 
#define case_inst(v0, v1, v2) \
break;}} case 0: {{inst_t _SW_tchk=_inst_t_tchk;}{char _inst_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_string_t* v1=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* v2=((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f2; 
#define case_inst_ptr(v0, v1, v2) \
break;}} case 0: {{inst_t _SW_tchk=_inst_t_tchk;}{char _inst_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_string_t* *v1=&((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_paramlist_t* *v2=&((struct _ADT_inst_t_inst*)((uintptr_t)_ADT_v-0))->f2; 

static __inline inst_t inst(struct _ADT_string_t* v0, struct _ADT_string_t* v1, struct _ADT_paramlist_t* v2){
	struct _ADT_inst_t_inst *v=(struct _ADT_inst_t_inst*)ADT_MALLOC(sizeof(struct _ADT_inst_t_inst));
	v->f0=v0; 
	v->f1=v1; 
	v->f2=v2; 
	return (inst_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_fn_t{} *fn_t;
#define fn_t_T_NUM_CONST (0) 
#define fn_t_T_NUM_NONCONST (1) 
#define fn_t_constructorNum(v) \
(( fn_t_T_NUM_NONCONST>0 && v >= fn_t_T_NUM_CONST ?  \
 ( fn_t_T_NUM_NONCONST==1 ?  fn_t_T_NUM_CONST   : \
(( fn_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + fn_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + fn_t_T_NUM_CONST)) : v ) )

static __inline void free_fn_t(fn_t v);
#define switch_fn_t(v) \
{fn_t _fn_t_tchk, _ADT_v=(v); \
int t = fn_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_fn_t(fn_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_fn_t_fn {
    struct _ADT_string_t* f0;
    struct _ADT_arglist_t* f1;
    struct _ADT_arglist_t* f2;
    struct _ADT_paramlist_t* f3;};
#define if_fn(v, v0, v1, v2, v3) \
{fn_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* v0=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_arglist_t* v2=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f2; \
struct _ADT_paramlist_t* v3=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f3; 
#define else_if_fn(v0, v1, v2, v3) \
} else if (1) {\
struct _ADT_string_t* v0=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_arglist_t* v2=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f2; \
struct _ADT_paramlist_t* v3=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f3; 
#define if_fn_ptr(v, v0, v1, v2, v3) \
{fn_t _ADT_v=(v);if (1) {\
struct _ADT_string_t* *v0=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_arglist_t* *v2=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f2; \
struct _ADT_paramlist_t* *v3=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f3; 
#define case_fn(v0, v1, v2, v3) \
break;}} case 0: {{fn_t _SW_tchk=_fn_t_tchk;}{char _fn_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_arglist_t* v2=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f2; \
struct _ADT_paramlist_t* v3=((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f3; 
#define case_fn_ptr(v0, v1, v2, v3) \
break;}} case 0: {{fn_t _SW_tchk=_fn_t_tchk;}{char _fn_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f1; \
struct _ADT_arglist_t* *v2=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f2; \
struct _ADT_paramlist_t* *v3=&((struct _ADT_fn_t_fn*)((uintptr_t)_ADT_v-0))->f3; 

static __inline fn_t fn(struct _ADT_string_t* v0, struct _ADT_arglist_t* v1, struct _ADT_arglist_t* v2, struct _ADT_paramlist_t* v3){
	struct _ADT_fn_t_fn *v=(struct _ADT_fn_t_fn*)ADT_MALLOC(sizeof(struct _ADT_fn_t_fn));
	v->f0=v0; 
	v->f1=v1; 
	v->f2=v2; 
	v->f3=v3; 
	return (fn_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_string_t{} *string_t;
#define string_t_T_NUM_CONST (0) 
#define string_t_T_NUM_NONCONST (1) 
#define string_t_constructorNum(v) \
(( string_t_T_NUM_NONCONST>0 && v >= string_t_T_NUM_CONST ?  \
 ( string_t_T_NUM_NONCONST==1 ?  string_t_T_NUM_CONST   : \
(( string_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + string_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + string_t_T_NUM_CONST)) : v ) )

static __inline void free_string_t(string_t v);
#define switch_string_t(v) \
{string_t _string_t_tchk, _ADT_v=(v); \
int t = string_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_string_t(string_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_string_t_string {
    c_string f0;};
#define if_string(v, v0) \
{string_t _ADT_v=(v);if (1) {\
c_string v0=((struct _ADT_string_t_string*)((uintptr_t)_ADT_v-0))->f0; 
#define else_if_string(v0) \
} else if (1) {\
c_string v0=((struct _ADT_string_t_string*)((uintptr_t)_ADT_v-0))->f0; 
#define if_string_ptr(v, v0) \
{string_t _ADT_v=(v);if (1) {\
c_string *v0=&((struct _ADT_string_t_string*)((uintptr_t)_ADT_v-0))->f0; 
#define case_string(v0) \
break;}} case 0: {{string_t _SW_tchk=_string_t_tchk;}{char _string_t_tchk; \
c_string v0=((struct _ADT_string_t_string*)((uintptr_t)_ADT_v-0))->f0; 
#define case_string_ptr(v0) \
break;}} case 0: {{string_t _SW_tchk=_string_t_tchk;}{char _string_t_tchk; \
c_string *v0=&((struct _ADT_string_t_string*)((uintptr_t)_ADT_v-0))->f0; 

static __inline string_t string(c_string v0){
	struct _ADT_string_t_string *v=(struct _ADT_string_t_string*)ADT_MALLOC(sizeof(struct _ADT_string_t_string));
	v->f0=v0; 
	return (string_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_t{} *list_t;
#define list_t_T_NUM_CONST (1) 
#define list_t_T_NUM_NONCONST (1) 
#define list_t_constructorNum(v) \
(( list_t_T_NUM_NONCONST>0 && v >= list_t_T_NUM_CONST ?  \
 ( list_t_T_NUM_NONCONST==1 ?  list_t_T_NUM_CONST   : \
(( list_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_t_T_NUM_CONST)) : v ) )

static __inline void free_list_t(list_t v);
#define switch_list_t(v) \
{list_t _list_t_tchk, _ADT_v=(v); \
int t = list_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_t(list_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_t_nil {};
#define if_nil(v) \
{list_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_ptr(v) \
{list_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil() \
break;}} case 0: {{list_t _SW_tchk=_list_t_tchk;}{char _list_t_tchk; 
#define case_nil_ptr() \
break;}} case 0: {{list_t _SW_tchk=_list_t_tchk;}{char _list_t_tchk; 

static __inline list_t nil(){
	struct _ADT_list_t_nil *v=(struct _ADT_list_t_nil*)0;
	return (list_t)((uintptr_t)v);
}
;
struct _ADT_list_t_cons {
    ADT_1 f0;
    struct _ADT_list_t* f1;};
#define if_cons(v, v0, v1) \
{list_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_t* v1=((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_t* v1=((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_ptr(v, v0, v1) \
{list_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 *v0=&((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_t* *v1=&((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons(v0, v1) \
break;}} case 1: {{list_t _SW_tchk=_list_t_tchk;}{char _list_t_tchk; \
ADT_1 v0=((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_t* v1=((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_ptr(v0, v1) \
break;}} case 1: {{list_t _SW_tchk=_list_t_tchk;}{char _list_t_tchk; \
ADT_1 *v0=&((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_t* *v1=&((struct _ADT_list_t_cons*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_t cons(ADT_1 v0, struct _ADT_list_t* v1){
	struct _ADT_list_t_cons *v=(struct _ADT_list_t_cons*)ADT_MALLOC(sizeof(struct _ADT_list_t_cons));
	v->f0=v0; 
	v->f1=v1; 
	return (list_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_fnlist_t{} *fnlist_t;
#define fnlist_t_T_NUM_CONST (1) 
#define fnlist_t_T_NUM_NONCONST (1) 
#define fnlist_t_constructorNum(v) \
(( fnlist_t_T_NUM_NONCONST>0 && v >= fnlist_t_T_NUM_CONST ?  \
 ( fnlist_t_T_NUM_NONCONST==1 ?  fnlist_t_T_NUM_CONST   : \
(( fnlist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + fnlist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + fnlist_t_T_NUM_CONST)) : v ) )

static __inline void free_fnlist_t(fnlist_t v);
#define switch_fnlist_t(v) \
{fnlist_t _fnlist_t_tchk, _ADT_v=(v); \
int t = fnlist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_fnlist_t(fnlist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_fnlist_t_nil_fnlist_t {};
#define if_nil_fnlist_t(v) \
{fnlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_fnlist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_fnlist_t_ptr(v) \
{fnlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_fnlist_t() \
break;}} case 0: {{fnlist_t _SW_tchk=_fnlist_t_tchk;}{char _fnlist_t_tchk; 
#define case_nil_fnlist_t_ptr() \
break;}} case 0: {{fnlist_t _SW_tchk=_fnlist_t_tchk;}{char _fnlist_t_tchk; 

static __inline fnlist_t nil_fnlist_t(){
	struct _ADT_fnlist_t_nil_fnlist_t *v=(struct _ADT_fnlist_t_nil_fnlist_t*)0;
	return (fnlist_t)((uintptr_t)v);
}
;
struct _ADT_fnlist_t_cons_fnlist_t {
    struct _ADT_fn_t* f0;
    struct _ADT_fnlist_t* f1;};
#define if_cons_fnlist_t(v, v0, v1) \
{fnlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_fn_t* v0=((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_fnlist_t* v1=((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_fnlist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_fn_t* v0=((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_fnlist_t* v1=((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_fnlist_t_ptr(v, v0, v1) \
{fnlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_fn_t* *v0=&((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_fnlist_t* *v1=&((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_fnlist_t(v0, v1) \
break;}} case 1: {{fnlist_t _SW_tchk=_fnlist_t_tchk;}{char _fnlist_t_tchk; \
struct _ADT_fn_t* v0=((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_fnlist_t* v1=((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_fnlist_t_ptr(v0, v1) \
break;}} case 1: {{fnlist_t _SW_tchk=_fnlist_t_tchk;}{char _fnlist_t_tchk; \
struct _ADT_fn_t* *v0=&((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_fnlist_t* *v1=&((struct _ADT_fnlist_t_cons_fnlist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline fnlist_t cons_fnlist_t(struct _ADT_fn_t* v0, struct _ADT_fnlist_t* v1){
	struct _ADT_fnlist_t_cons_fnlist_t *v=(struct _ADT_fnlist_t_cons_fnlist_t*)ADT_MALLOC(sizeof(struct _ADT_fnlist_t_cons_fnlist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (fnlist_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_stringlist_t{} *stringlist_t;
#define stringlist_t_T_NUM_CONST (1) 
#define stringlist_t_T_NUM_NONCONST (1) 
#define stringlist_t_constructorNum(v) \
(( stringlist_t_T_NUM_NONCONST>0 && v >= stringlist_t_T_NUM_CONST ?  \
 ( stringlist_t_T_NUM_NONCONST==1 ?  stringlist_t_T_NUM_CONST   : \
(( stringlist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + stringlist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + stringlist_t_T_NUM_CONST)) : v ) )

static __inline void free_stringlist_t(stringlist_t v);
#define switch_stringlist_t(v) \
{stringlist_t _stringlist_t_tchk, _ADT_v=(v); \
int t = stringlist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_stringlist_t(stringlist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_stringlist_t_nil_stringlist_t {};
#define if_nil_stringlist_t(v) \
{stringlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_stringlist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_stringlist_t_ptr(v) \
{stringlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_stringlist_t() \
break;}} case 0: {{stringlist_t _SW_tchk=_stringlist_t_tchk;}{char _stringlist_t_tchk; 
#define case_nil_stringlist_t_ptr() \
break;}} case 0: {{stringlist_t _SW_tchk=_stringlist_t_tchk;}{char _stringlist_t_tchk; 

static __inline stringlist_t nil_stringlist_t(){
	struct _ADT_stringlist_t_nil_stringlist_t *v=(struct _ADT_stringlist_t_nil_stringlist_t*)0;
	return (stringlist_t)((uintptr_t)v);
}
;
struct _ADT_stringlist_t_cons_stringlist_t {
    struct _ADT_string_t* f0;
    struct _ADT_stringlist_t* f1;};
#define if_cons_stringlist_t(v, v0, v1) \
{stringlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_string_t* v0=((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist_t* v1=((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_stringlist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_string_t* v0=((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist_t* v1=((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_stringlist_t_ptr(v, v0, v1) \
{stringlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_string_t* *v0=&((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist_t* *v1=&((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_stringlist_t(v0, v1) \
break;}} case 1: {{stringlist_t _SW_tchk=_stringlist_t_tchk;}{char _stringlist_t_tchk; \
struct _ADT_string_t* v0=((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist_t* v1=((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_stringlist_t_ptr(v0, v1) \
break;}} case 1: {{stringlist_t _SW_tchk=_stringlist_t_tchk;}{char _stringlist_t_tchk; \
struct _ADT_string_t* *v0=&((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist_t* *v1=&((struct _ADT_stringlist_t_cons_stringlist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline stringlist_t cons_stringlist_t(struct _ADT_string_t* v0, struct _ADT_stringlist_t* v1){
	struct _ADT_stringlist_t_cons_stringlist_t *v=(struct _ADT_stringlist_t_cons_stringlist_t*)ADT_MALLOC(sizeof(struct _ADT_stringlist_t_cons_stringlist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (stringlist_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_paramlist_t{} *paramlist_t;
#define paramlist_t_T_NUM_CONST (1) 
#define paramlist_t_T_NUM_NONCONST (1) 
#define paramlist_t_constructorNum(v) \
(( paramlist_t_T_NUM_NONCONST>0 && v >= paramlist_t_T_NUM_CONST ?  \
 ( paramlist_t_T_NUM_NONCONST==1 ?  paramlist_t_T_NUM_CONST   : \
(( paramlist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + paramlist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + paramlist_t_T_NUM_CONST)) : v ) )

static __inline void free_paramlist_t(paramlist_t v);
#define switch_paramlist_t(v) \
{paramlist_t _paramlist_t_tchk, _ADT_v=(v); \
int t = paramlist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_paramlist_t(paramlist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_paramlist_t_nil_paramlist_t {};
#define if_nil_paramlist_t(v) \
{paramlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_paramlist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_paramlist_t_ptr(v) \
{paramlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_paramlist_t() \
break;}} case 0: {{paramlist_t _SW_tchk=_paramlist_t_tchk;}{char _paramlist_t_tchk; 
#define case_nil_paramlist_t_ptr() \
break;}} case 0: {{paramlist_t _SW_tchk=_paramlist_t_tchk;}{char _paramlist_t_tchk; 

static __inline paramlist_t nil_paramlist_t(){
	struct _ADT_paramlist_t_nil_paramlist_t *v=(struct _ADT_paramlist_t_nil_paramlist_t*)0;
	return (paramlist_t)((uintptr_t)v);
}
;
struct _ADT_paramlist_t_cons_paramlist_t {
    struct _ADT_param_t* f0;
    struct _ADT_paramlist_t* f1;};
#define if_cons_paramlist_t(v, v0, v1) \
{paramlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_param_t* v0=((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_paramlist_t* v1=((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_paramlist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_param_t* v0=((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_paramlist_t* v1=((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_paramlist_t_ptr(v, v0, v1) \
{paramlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_param_t* *v0=&((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_paramlist_t* *v1=&((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_paramlist_t(v0, v1) \
break;}} case 1: {{paramlist_t _SW_tchk=_paramlist_t_tchk;}{char _paramlist_t_tchk; \
struct _ADT_param_t* v0=((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_paramlist_t* v1=((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_paramlist_t_ptr(v0, v1) \
break;}} case 1: {{paramlist_t _SW_tchk=_paramlist_t_tchk;}{char _paramlist_t_tchk; \
struct _ADT_param_t* *v0=&((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_paramlist_t* *v1=&((struct _ADT_paramlist_t_cons_paramlist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline paramlist_t cons_paramlist_t(struct _ADT_param_t* v0, struct _ADT_paramlist_t* v1){
	struct _ADT_paramlist_t_cons_paramlist_t *v=(struct _ADT_paramlist_t_cons_paramlist_t*)ADT_MALLOC(sizeof(struct _ADT_paramlist_t_cons_paramlist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (paramlist_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_instlist_t{} *instlist_t;
#define instlist_t_T_NUM_CONST (1) 
#define instlist_t_T_NUM_NONCONST (1) 
#define instlist_t_constructorNum(v) \
(( instlist_t_T_NUM_NONCONST>0 && v >= instlist_t_T_NUM_CONST ?  \
 ( instlist_t_T_NUM_NONCONST==1 ?  instlist_t_T_NUM_CONST   : \
(( instlist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + instlist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + instlist_t_T_NUM_CONST)) : v ) )

static __inline void free_instlist_t(instlist_t v);
#define switch_instlist_t(v) \
{instlist_t _instlist_t_tchk, _ADT_v=(v); \
int t = instlist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_instlist_t(instlist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_instlist_t_nil_instlist_t {};
#define if_nil_instlist_t(v) \
{instlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_instlist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_instlist_t_ptr(v) \
{instlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_instlist_t() \
break;}} case 0: {{instlist_t _SW_tchk=_instlist_t_tchk;}{char _instlist_t_tchk; 
#define case_nil_instlist_t_ptr() \
break;}} case 0: {{instlist_t _SW_tchk=_instlist_t_tchk;}{char _instlist_t_tchk; 

static __inline instlist_t nil_instlist_t(){
	struct _ADT_instlist_t_nil_instlist_t *v=(struct _ADT_instlist_t_nil_instlist_t*)0;
	return (instlist_t)((uintptr_t)v);
}
;
struct _ADT_instlist_t_cons_instlist_t {
    struct _ADT_inst_t* f0;
    struct _ADT_instlist_t* f1;};
#define if_cons_instlist_t(v, v0, v1) \
{instlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_inst_t* v0=((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_instlist_t* v1=((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_instlist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_inst_t* v0=((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_instlist_t* v1=((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_instlist_t_ptr(v, v0, v1) \
{instlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_inst_t* *v0=&((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_instlist_t* *v1=&((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_instlist_t(v0, v1) \
break;}} case 1: {{instlist_t _SW_tchk=_instlist_t_tchk;}{char _instlist_t_tchk; \
struct _ADT_inst_t* v0=((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_instlist_t* v1=((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_instlist_t_ptr(v0, v1) \
break;}} case 1: {{instlist_t _SW_tchk=_instlist_t_tchk;}{char _instlist_t_tchk; \
struct _ADT_inst_t* *v0=&((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_instlist_t* *v1=&((struct _ADT_instlist_t_cons_instlist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline instlist_t cons_instlist_t(struct _ADT_inst_t* v0, struct _ADT_instlist_t* v1){
	struct _ADT_instlist_t_cons_instlist_t *v=(struct _ADT_instlist_t_cons_instlist_t*)ADT_MALLOC(sizeof(struct _ADT_instlist_t_cons_instlist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (instlist_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_arglist_t{} *arglist_t;
#define arglist_t_T_NUM_CONST (1) 
#define arglist_t_T_NUM_NONCONST (1) 
#define arglist_t_constructorNum(v) \
(( arglist_t_T_NUM_NONCONST>0 && v >= arglist_t_T_NUM_CONST ?  \
 ( arglist_t_T_NUM_NONCONST==1 ?  arglist_t_T_NUM_CONST   : \
(( arglist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + arglist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + arglist_t_T_NUM_CONST)) : v ) )

static __inline void free_arglist_t(arglist_t v);
#define switch_arglist_t(v) \
{arglist_t _arglist_t_tchk, _ADT_v=(v); \
int t = arglist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_arglist_t(arglist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_arglist_t_nil_arglist_t {};
#define if_nil_arglist_t(v) \
{arglist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_arglist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_arglist_t_ptr(v) \
{arglist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_arglist_t() \
break;}} case 0: {{arglist_t _SW_tchk=_arglist_t_tchk;}{char _arglist_t_tchk; 
#define case_nil_arglist_t_ptr() \
break;}} case 0: {{arglist_t _SW_tchk=_arglist_t_tchk;}{char _arglist_t_tchk; 

static __inline arglist_t nil_arglist_t(){
	struct _ADT_arglist_t_nil_arglist_t *v=(struct _ADT_arglist_t_nil_arglist_t*)0;
	return (arglist_t)((uintptr_t)v);
}
;
struct _ADT_arglist_t_cons_arglist_t {
    struct _ADT_arg_t* f0;
    struct _ADT_arglist_t* f1;};
#define if_cons_arglist_t(v, v0, v1) \
{arglist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_arg_t* v0=((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_arglist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_arg_t* v0=((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_arglist_t_ptr(v, v0, v1) \
{arglist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_arg_t* *v0=&((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_arglist_t(v0, v1) \
break;}} case 1: {{arglist_t _SW_tchk=_arglist_t_tchk;}{char _arglist_t_tchk; \
struct _ADT_arg_t* v0=((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* v1=((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_arglist_t_ptr(v0, v1) \
break;}} case 1: {{arglist_t _SW_tchk=_arglist_t_tchk;}{char _arglist_t_tchk; \
struct _ADT_arg_t* *v0=&((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_arglist_t* *v1=&((struct _ADT_arglist_t_cons_arglist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline arglist_t cons_arglist_t(struct _ADT_arg_t* v0, struct _ADT_arglist_t* v1){
	struct _ADT_arglist_t_cons_arglist_t *v=(struct _ADT_arglist_t_cons_arglist_t*)ADT_MALLOC(sizeof(struct _ADT_arglist_t_cons_arglist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (arglist_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_ctrlist_t{} *ctrlist_t;
#define ctrlist_t_T_NUM_CONST (1) 
#define ctrlist_t_T_NUM_NONCONST (1) 
#define ctrlist_t_constructorNum(v) \
(( ctrlist_t_T_NUM_NONCONST>0 && v >= ctrlist_t_T_NUM_CONST ?  \
 ( ctrlist_t_T_NUM_NONCONST==1 ?  ctrlist_t_T_NUM_CONST   : \
(( ctrlist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + ctrlist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + ctrlist_t_T_NUM_CONST)) : v ) )

static __inline void free_ctrlist_t(ctrlist_t v);
#define switch_ctrlist_t(v) \
{ctrlist_t _ctrlist_t_tchk, _ADT_v=(v); \
int t = ctrlist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_ctrlist_t(ctrlist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_ctrlist_t_nil_ctrlist_t {};
#define if_nil_ctrlist_t(v) \
{ctrlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_ctrlist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_ctrlist_t_ptr(v) \
{ctrlist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_ctrlist_t() \
break;}} case 0: {{ctrlist_t _SW_tchk=_ctrlist_t_tchk;}{char _ctrlist_t_tchk; 
#define case_nil_ctrlist_t_ptr() \
break;}} case 0: {{ctrlist_t _SW_tchk=_ctrlist_t_tchk;}{char _ctrlist_t_tchk; 

static __inline ctrlist_t nil_ctrlist_t(){
	struct _ADT_ctrlist_t_nil_ctrlist_t *v=(struct _ADT_ctrlist_t_nil_ctrlist_t*)0;
	return (ctrlist_t)((uintptr_t)v);
}
;
struct _ADT_ctrlist_t_cons_ctrlist_t {
    struct _ADT_ctr_t* f0;
    struct _ADT_ctrlist_t* f1;};
#define if_cons_ctrlist_t(v, v0, v1) \
{ctrlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_ctr_t* v0=((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_ctrlist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_ctr_t* v0=((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_ctrlist_t_ptr(v, v0, v1) \
{ctrlist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_ctr_t* *v0=&((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* *v1=&((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_ctrlist_t(v0, v1) \
break;}} case 1: {{ctrlist_t _SW_tchk=_ctrlist_t_tchk;}{char _ctrlist_t_tchk; \
struct _ADT_ctr_t* v0=((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* v1=((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_ctrlist_t_ptr(v0, v1) \
break;}} case 1: {{ctrlist_t _SW_tchk=_ctrlist_t_tchk;}{char _ctrlist_t_tchk; \
struct _ADT_ctr_t* *v0=&((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_ctrlist_t* *v1=&((struct _ADT_ctrlist_t_cons_ctrlist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline ctrlist_t cons_ctrlist_t(struct _ADT_ctr_t* v0, struct _ADT_ctrlist_t* v1){
	struct _ADT_ctrlist_t_cons_ctrlist_t *v=(struct _ADT_ctrlist_t_cons_ctrlist_t*)ADT_MALLOC(sizeof(struct _ADT_ctrlist_t_cons_ctrlist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (ctrlist_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_decllist_t{} *decllist_t;
#define decllist_t_T_NUM_CONST (1) 
#define decllist_t_T_NUM_NONCONST (1) 
#define decllist_t_constructorNum(v) \
(( decllist_t_T_NUM_NONCONST>0 && v >= decllist_t_T_NUM_CONST ?  \
 ( decllist_t_T_NUM_NONCONST==1 ?  decllist_t_T_NUM_CONST   : \
(( decllist_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + decllist_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + decllist_t_T_NUM_CONST)) : v ) )

static __inline void free_decllist_t(decllist_t v);
#define switch_decllist_t(v) \
{decllist_t _decllist_t_tchk, _ADT_v=(v); \
int t = decllist_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_decllist_t(decllist_t v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_decllist_t_nil_decllist_t {};
#define if_nil_decllist_t(v) \
{decllist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_decllist_t() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_decllist_t_ptr(v) \
{decllist_t _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_decllist_t() \
break;}} case 0: {{decllist_t _SW_tchk=_decllist_t_tchk;}{char _decllist_t_tchk; 
#define case_nil_decllist_t_ptr() \
break;}} case 0: {{decllist_t _SW_tchk=_decllist_t_tchk;}{char _decllist_t_tchk; 

static __inline decllist_t nil_decllist_t(){
	struct _ADT_decllist_t_nil_decllist_t *v=(struct _ADT_decllist_t_nil_decllist_t*)0;
	return (decllist_t)((uintptr_t)v);
}
;
struct _ADT_decllist_t_cons_decllist_t {
    struct _ADT_decl_t* f0;
    struct _ADT_decllist_t* f1;};
#define if_cons_decllist_t(v, v0, v1) \
{decllist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_decl_t* v0=((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_decllist_t* v1=((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_decllist_t(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_decl_t* v0=((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_decllist_t* v1=((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_decllist_t_ptr(v, v0, v1) \
{decllist_t _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_decl_t* *v0=&((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_decllist_t* *v1=&((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_decllist_t(v0, v1) \
break;}} case 1: {{decllist_t _SW_tchk=_decllist_t_tchk;}{char _decllist_t_tchk; \
struct _ADT_decl_t* v0=((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_decllist_t* v1=((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_decllist_t_ptr(v0, v1) \
break;}} case 1: {{decllist_t _SW_tchk=_decllist_t_tchk;}{char _decllist_t_tchk; \
struct _ADT_decl_t* *v0=&((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_decllist_t* *v1=&((struct _ADT_decllist_t_cons_decllist_t*)((uintptr_t)_ADT_v-0))->f1; 

static __inline decllist_t cons_decllist_t(struct _ADT_decl_t* v0, struct _ADT_decllist_t* v1){
	struct _ADT_decllist_t_cons_decllist_t *v=(struct _ADT_decllist_t_cons_decllist_t*)ADT_MALLOC(sizeof(struct _ADT_decllist_t_cons_decllist_t));
	v->f0=v0; 
	v->f1=v1; 
	return (decllist_t)(0+(uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/

int length(list_t);
list_t concat_lists(list_t, list_t);
list_t reverse_list(list_t);

/*----------------------------------------------------------------------------*/

/* Function Aliases */
/*----------------------------------------------------------------------------*/

static __inline instlist_t concat_inst_list(instlist_t v0, instlist_t v1){
    return (instlist_t) concat_lists((list_t) v0, (list_t) v1);
}

static __inline int length_paramlist(paramlist_t v0){
    return (int) length((list_t) v0);
}

static __inline paramlist_t reverse_paramlist(paramlist_t v0){
    return (paramlist_t) reverse_list((list_t) v0);
}

static __inline arglist_t reverse_arglist(arglist_t v0){
    return (arglist_t) reverse_list((list_t) v0);
}

static __inline ctrlist_t reverse_ctrlist(ctrlist_t v0){
    return (ctrlist_t) reverse_list((list_t) v0);
}

static __inline stringlist_t reverse_stringlist(stringlist_t v0){
    return (stringlist_t) reverse_list((list_t) v0);
}


/*----------------------------------------------------------------------------*/
