#include<stdio.h>
#include "ctest.h"

list reverse_list(list input){
    list temp_list;
    temp_list = nil();
    while(1){
        if_cons(input, head, tail){
            temp_list = cons(head, temp_list);
            input = tail;
        end_if()

        if_nil(input)
            return temp_list;
        end_if()
        }
    }
}

int length(list hi){
    if_cons(hi, hi_head, hi_tail)
        return 1 + length(hi_tail);
    else()
        return 0;
    end_if()
}

void print_list_char(list_char a){
    if_cons_list_char(a, ahead, atail)
        printf("%c\n", ahead);
        print_list_char(atail);
    end_if()
}

void main(){
    list_char char_list = cons_list_char('a', cons_list_char('b', cons_list_char('c', nil_list_char())));
    list_int int_list = cons_list_int(1, cons_list_int(2, cons_list_int(3, nil_list_int())));
    list_list_char list_char_list = cons_list_list_char(char_list, nil_list_list_char());
    
    list_char char_int;
    int int_int;
    int list_char_int;
    
    char_int = reverse_list_char(char_list);
    
    print_list_char(char_int);
    
    exit(0);
}