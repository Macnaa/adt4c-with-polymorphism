#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_fred{} *fred;
#define fred_T_NUM_CONST (0) 
#define fred_T_NUM_NONCONST (1) 
#define fred_constructorNum(v) \
(( fred_T_NUM_NONCONST>0 && v >= fred_T_NUM_CONST ?  \
 ( fred_T_NUM_NONCONST==1 ?  fred_T_NUM_CONST   : \
(( fred_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + fred_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + fred_T_NUM_CONST)) : v ) )

static __inline void free_fred(fred v);
#define switch_fred(v) \
{fred _fred_tchk, _ADT_v=(v); \
int t = fred_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_fred(fred v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_fred_freddy {
    struct _ADT_list_char* f0;};
#define if_freddy(v, v0) \
{fred _ADT_v=(v);if (1) {\
struct _ADT_list_char* v0=((struct _ADT_fred_freddy*)((uintptr_t)_ADT_v-0))->f0; 
#define else_if_freddy(v0) \
} else if (1) {\
struct _ADT_list_char* v0=((struct _ADT_fred_freddy*)((uintptr_t)_ADT_v-0))->f0; 
#define if_freddy_ptr(v, v0) \
{fred _ADT_v=(v);if (1) {\
struct _ADT_list_char* *v0=&((struct _ADT_fred_freddy*)((uintptr_t)_ADT_v-0))->f0; 
#define case_freddy(v0) \
break;}} case 0: {{fred _SW_tchk=_fred_tchk;}{char _fred_tchk; \
struct _ADT_list_char* v0=((struct _ADT_fred_freddy*)((uintptr_t)_ADT_v-0))->f0; 
#define case_freddy_ptr(v0) \
break;}} case 0: {{fred _SW_tchk=_fred_tchk;}{char _fred_tchk; \
struct _ADT_list_char* *v0=&((struct _ADT_fred_freddy*)((uintptr_t)_ADT_v-0))->f0; 

static __inline fred freddy(struct _ADT_list_char* v0){
	struct _ADT_fred_freddy *v=(struct _ADT_fred_freddy*)ADT_MALLOC(sizeof(struct _ADT_fred_freddy));
	v->f0=v0; 
	return (fred)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_list{} *list;
#define list_T_NUM_CONST (1) 
#define list_T_NUM_NONCONST (1) 
#define list_constructorNum(v) \
(( list_T_NUM_NONCONST>0 && v >= list_T_NUM_CONST ?  \
 ( list_T_NUM_NONCONST==1 ?  list_T_NUM_CONST   : \
(( list_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_T_NUM_CONST)) : v ) )

static __inline void free_list(list v);
#define switch_list(v) \
{list _list_tchk, _ADT_v=(v); \
int t = list_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list(list v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_nil {};
#define if_nil(v) \
{list _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_ptr(v) \
{list _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil() \
break;}} case 0: {{list _SW_tchk=_list_tchk;}{char _list_tchk; 
#define case_nil_ptr() \
break;}} case 0: {{list _SW_tchk=_list_tchk;}{char _list_tchk; 

static __inline list nil(){
	struct _ADT_list_nil *v=(struct _ADT_list_nil*)0;
	return (list)((uintptr_t)v);
}
;
struct _ADT_list_cons {
    ADT_1 f0;
    struct _ADT_list* f1;};
#define if_cons(v, v0, v1) \
{list _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_ptr(v, v0, v1) \
{list _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 *v0=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* *v1=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons(v0, v1) \
break;}} case 1: {{list _SW_tchk=_list_tchk;}{char _list_tchk; \
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_ptr(v0, v1) \
break;}} case 1: {{list _SW_tchk=_list_tchk;}{char _list_tchk; \
ADT_1 *v0=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* *v1=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list cons(ADT_1 v0, struct _ADT_list* v1){
	struct _ADT_list_cons *v=(struct _ADT_list_cons*)ADT_MALLOC(sizeof(struct _ADT_list_cons));
	v->f0=v0; 
	v->f1=v1; 
	return (list)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_char{} *list_char;
#define list_char_T_NUM_CONST (1) 
#define list_char_T_NUM_NONCONST (1) 
#define list_char_constructorNum(v) \
(( list_char_T_NUM_NONCONST>0 && v >= list_char_T_NUM_CONST ?  \
 ( list_char_T_NUM_NONCONST==1 ?  list_char_T_NUM_CONST   : \
(( list_char_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_char_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_char_T_NUM_CONST)) : v ) )

static __inline void free_list_char(list_char v);
#define switch_list_char(v) \
{list_char _list_char_tchk, _ADT_v=(v); \
int t = list_char_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_char(list_char v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_char_nil_list_char {};
#define if_nil_list_char(v) \
{list_char _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list_char() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list_char_ptr(v) \
{list_char _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list_char() \
break;}} case 0: {{list_char _SW_tchk=_list_char_tchk;}{char _list_char_tchk; 
#define case_nil_list_char_ptr() \
break;}} case 0: {{list_char _SW_tchk=_list_char_tchk;}{char _list_char_tchk; 

static __inline list_char nil_list_char(){
	struct _ADT_list_char_nil_list_char *v=(struct _ADT_list_char_nil_list_char*)0;
	return (list_char)((uintptr_t)v);
}
;
struct _ADT_list_char_cons_list_char {
    char f0;
    struct _ADT_list_char* f1;};
#define if_cons_list_char(v, v0, v1) \
{list_char _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
char v0=((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_char* v1=((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list_char(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
char v0=((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_char* v1=((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list_char_ptr(v, v0, v1) \
{list_char _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
char *v0=&((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_char* *v1=&((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_char(v0, v1) \
break;}} case 1: {{list_char _SW_tchk=_list_char_tchk;}{char _list_char_tchk; \
char v0=((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_char* v1=((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_char_ptr(v0, v1) \
break;}} case 1: {{list_char _SW_tchk=_list_char_tchk;}{char _list_char_tchk; \
char *v0=&((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_char* *v1=&((struct _ADT_list_char_cons_list_char*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_char cons_list_char(char v0, struct _ADT_list_char* v1){
	struct _ADT_list_char_cons_list_char *v=(struct _ADT_list_char_cons_list_char*)ADT_MALLOC(sizeof(struct _ADT_list_char_cons_list_char));
	v->f0=v0; 
	v->f1=v1; 
	return (list_char)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_int{} *list_int;
#define list_int_T_NUM_CONST (1) 
#define list_int_T_NUM_NONCONST (1) 
#define list_int_constructorNum(v) \
(( list_int_T_NUM_NONCONST>0 && v >= list_int_T_NUM_CONST ?  \
 ( list_int_T_NUM_NONCONST==1 ?  list_int_T_NUM_CONST   : \
(( list_int_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_int_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_int_T_NUM_CONST)) : v ) )

static __inline void free_list_int(list_int v);
#define switch_list_int(v) \
{list_int _list_int_tchk, _ADT_v=(v); \
int t = list_int_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_int(list_int v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_int_nil_list_int {};
#define if_nil_list_int(v) \
{list_int _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list_int() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list_int_ptr(v) \
{list_int _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list_int() \
break;}} case 0: {{list_int _SW_tchk=_list_int_tchk;}{char _list_int_tchk; 
#define case_nil_list_int_ptr() \
break;}} case 0: {{list_int _SW_tchk=_list_int_tchk;}{char _list_int_tchk; 

static __inline list_int nil_list_int(){
	struct _ADT_list_int_nil_list_int *v=(struct _ADT_list_int_nil_list_int*)0;
	return (list_int)((uintptr_t)v);
}
;
struct _ADT_list_int_cons_list_int {
    int f0;
    struct _ADT_list_int* f1;};
#define if_cons_list_int(v, v0, v1) \
{list_int _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
int v0=((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_int* v1=((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list_int(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
int v0=((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_int* v1=((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list_int_ptr(v, v0, v1) \
{list_int _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
int *v0=&((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_int* *v1=&((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_int(v0, v1) \
break;}} case 1: {{list_int _SW_tchk=_list_int_tchk;}{char _list_int_tchk; \
int v0=((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_int* v1=((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_int_ptr(v0, v1) \
break;}} case 1: {{list_int _SW_tchk=_list_int_tchk;}{char _list_int_tchk; \
int *v0=&((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_int* *v1=&((struct _ADT_list_int_cons_list_int*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_int cons_list_int(int v0, struct _ADT_list_int* v1){
	struct _ADT_list_int_cons_list_int *v=(struct _ADT_list_int_cons_list_int*)ADT_MALLOC(sizeof(struct _ADT_list_int_cons_list_int));
	v->f0=v0; 
	v->f1=v1; 
	return (list_int)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_list_char{} *list_list_char;
#define list_list_char_T_NUM_CONST (1) 
#define list_list_char_T_NUM_NONCONST (1) 
#define list_list_char_constructorNum(v) \
(( list_list_char_T_NUM_NONCONST>0 && v >= list_list_char_T_NUM_CONST ?  \
 ( list_list_char_T_NUM_NONCONST==1 ?  list_list_char_T_NUM_CONST   : \
(( list_list_char_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_list_char_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_list_char_T_NUM_CONST)) : v ) )

static __inline void free_list_list_char(list_list_char v);
#define switch_list_list_char(v) \
{list_list_char _list_list_char_tchk, _ADT_v=(v); \
int t = list_list_char_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_list_char(list_list_char v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_list_char_nil_list_list_char {};
#define if_nil_list_list_char(v) \
{list_list_char _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list_list_char() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list_list_char_ptr(v) \
{list_list_char _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list_list_char() \
break;}} case 0: {{list_list_char _SW_tchk=_list_list_char_tchk;}{char _list_list_char_tchk; 
#define case_nil_list_list_char_ptr() \
break;}} case 0: {{list_list_char _SW_tchk=_list_list_char_tchk;}{char _list_list_char_tchk; 

static __inline list_list_char nil_list_list_char(){
	struct _ADT_list_list_char_nil_list_list_char *v=(struct _ADT_list_list_char_nil_list_list_char*)0;
	return (list_list_char)((uintptr_t)v);
}
;
struct _ADT_list_list_char_cons_list_list_char {
    struct _ADT_list_char* f0;
    struct _ADT_list_list_char* f1;};
#define if_cons_list_list_char(v, v0, v1) \
{list_list_char _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_list_char* v0=((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_list_char* v1=((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list_list_char(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_list_char* v0=((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_list_char* v1=((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list_list_char_ptr(v, v0, v1) \
{list_list_char _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
struct _ADT_list_char* *v0=&((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_list_char* *v1=&((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_list_char(v0, v1) \
break;}} case 1: {{list_list_char _SW_tchk=_list_list_char_tchk;}{char _list_list_char_tchk; \
struct _ADT_list_char* v0=((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_list_char* v1=((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_list_char_ptr(v0, v1) \
break;}} case 1: {{list_list_char _SW_tchk=_list_list_char_tchk;}{char _list_list_char_tchk; \
struct _ADT_list_char* *v0=&((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_list_char* *v1=&((struct _ADT_list_list_char_cons_list_list_char*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_list_char cons_list_list_char(struct _ADT_list_char* v0, struct _ADT_list_list_char* v1){
	struct _ADT_list_list_char_cons_list_list_char *v=(struct _ADT_list_list_char_cons_list_list_char*)ADT_MALLOC(sizeof(struct _ADT_list_list_char_cons_list_list_char));
	v->f0=v0; 
	v->f1=v1; 
	return (list_list_char)(0+(uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/

list reverse_list(list);
int length(list);

/* Function Aliases */
/*----------------------------------------------------------------------------*/

static __inline list_list_char reverse_list_list_char(list_list_char v0){
    return (list_list_char) reverse_list((list) v0);
}

static __inline list_char reverse_list_char(list_char v0){
    return (list_char) reverse_list((list) v0);
}

static __inline int length_list_char(list_list_char v0){
    return (int) length((list) v0);
}

static __inline int length_char(list_char v0){
    return (int) length((list) v0);
}

static __inline int length_int(list_int v0){
    return (int) length((list) v0);
}

/*----------------------------------------------------------------------------*/
