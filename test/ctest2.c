#include<stdio.h>
#include "ctest2.h"

swapped swap_pair(pair_t p){ 
    swapped s_p; 

    if_pair(p, first, second)
        s_p = pair_swapped(second, first);
        return s_p;
    end_if()
}

void main(){
    char_int ci = pair_char_int("hello",123);
    int_char ic;
    ic = swap_char_int(ci);
    
    if_pair_int_char(ic, i, c)
        printf("%ld\n", i);
        printf("%s\n", c);
    end_if()
    
    exit(0);
}