#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
typedef char *ADT_string;
typedef intptr_t ADT_int;
typedef intptr_t ADT_char;
typedef uintptr_t ADT_uint;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_hello{} *hello;
#define hello_T_NUM_CONST (1) 
#define hello_T_NUM_NONCONST (2) 
#define hello_constructorNum(v) \
(( hello_T_NUM_NONCONST>0 && v >= hello_T_NUM_CONST ?  \
 ( hello_T_NUM_NONCONST==1 ?  hello_T_NUM_CONST   : \
(( hello_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + hello_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + hello_T_NUM_CONST)) : v ) )

static __inline void free_hello(hello v);
#define switch_hello(v) \
{hello _hello_tchk, _ADT_v=(v); \
int t = hello_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_hello(hello v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v&~LOW_BIT_USED));
	}
}

;
struct _ADT_hello_Cons {
    c_string f0;
    ADT_1 (*(*f1)(ADT_3))(ADT_2);
    c_string f2;
    ADT_1 (*f3)(ADT_2);};
#define if_Cons(v, v0, v1, v2, v3) \
{hello _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0) {\
c_string v0=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 (*(*v1)(ADT_3))(ADT_2)=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f1; \
c_string v2=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f2; \
ADT_1 (*v3)(ADT_2)=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f3; 
#define else_if_Cons(v0, v1, v2, v3) \
} else if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
c_string v0=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 (*(*v1)(ADT_3))(ADT_2)=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f1; \
c_string v2=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f2; \
ADT_1 (*v3)(ADT_2)=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f3; 
#define if_Cons_ptr(v, v0, v1, v2, v3) \
{hello _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
c_string *v0=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 (*(**v1)(ADT_3))(ADT_2)=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f1; \
c_string *v2=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f2; \
ADT_1 (**v3)(ADT_2)=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f3; 
#define case_Cons(v0, v1, v2, v3) \
break;}} case 1: {{hello _SW_tchk=_hello_tchk;}{char _hello_tchk; \
c_string v0=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 (*(*v1)(ADT_3))(ADT_2)=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f1; \
c_string v2=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f2; \
ADT_1 (*v3)(ADT_2)=((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f3; 
#define case_Cons_ptr(v0, v1, v2, v3) \
break;}} case 1: {{hello _SW_tchk=_hello_tchk;}{char _hello_tchk; \
c_string *v0=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 (*(**v1)(ADT_3))(ADT_2)=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f1; \
c_string *v2=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f2; \
ADT_1 (**v3)(ADT_2)=&((struct _ADT_hello_Cons*)((uintptr_t)_ADT_v-0))->f3; 

static __inline hello Cons(c_string v0, ADT_1 (*(*v1)(ADT_3))(ADT_2), c_string v2, ADT_1 (*v3)(ADT_2)){
	struct _ADT_hello_Cons *v=(struct _ADT_hello_Cons*)ADT_MALLOC(sizeof(struct _ADT_hello_Cons));
	v->f0=v0; 
	v->f1=v1; 
	v->f2=v2; 
	v->f3=v3; 
	return (hello)(0+(uintptr_t)v);
}
;
struct _ADT_hello_nil {};
#define if_nil(v) \
{hello _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_ptr(v) \
{hello _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil() \
break;}} case 0: {{hello _SW_tchk=_hello_tchk;}{char _hello_tchk; 
#define case_nil_ptr() \
break;}} case 0: {{hello _SW_tchk=_hello_tchk;}{char _hello_tchk; 

static __inline hello nil(){
	struct _ADT_hello_nil *v=(struct _ADT_hello_nil*)0;
	return (hello)((uintptr_t)v);
}
;
struct _ADT_hello_Hi {
    ADT_2 (*f0)(ADT_3);
    c_string f1;};
#define if_Hi(v, v0, v1) \
{hello _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1) {\
ADT_2 (*v0)(ADT_3)=((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f0; \
c_string v1=((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f1; 
#define else_if_Hi(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
ADT_2 (*v0)(ADT_3)=((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f0; \
c_string v1=((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f1; 
#define if_Hi_ptr(v, v0, v1) \
{hello _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
ADT_2 (**v0)(ADT_3)=&((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f0; \
c_string *v1=&((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f1; 
#define case_Hi(v0, v1) \
break;}} case 2: {{hello _SW_tchk=_hello_tchk;}{char _hello_tchk; \
ADT_2 (*v0)(ADT_3)=((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f0; \
c_string v1=((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f1; 
#define case_Hi_ptr(v0, v1) \
break;}} case 2: {{hello _SW_tchk=_hello_tchk;}{char _hello_tchk; \
ADT_2 (**v0)(ADT_3)=&((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f0; \
c_string *v1=&((struct _ADT_hello_Hi*)((uintptr_t)_ADT_v-1))->f1; 

static __inline hello Hi(ADT_2 (*v0)(ADT_3), c_string v1){
	struct _ADT_hello_Hi *v=(struct _ADT_hello_Hi*)ADT_MALLOC(sizeof(struct _ADT_hello_Hi));
	v->f0=v0; 
	v->f1=v1; 
	return (hello)(1+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_hello_1{} *hello_1;
#define hello_1_T_NUM_CONST (1) 
#define hello_1_T_NUM_NONCONST (2) 
#define hello_1_constructorNum(v) \
(( hello_1_T_NUM_NONCONST>0 && v >= hello_1_T_NUM_CONST ?  \
 ( hello_1_T_NUM_NONCONST==1 ?  hello_1_T_NUM_CONST   : \
(( hello_1_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + hello_1_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + hello_1_T_NUM_CONST)) : v ) )

static __inline void free_hello_1(hello_1 v);
#define switch_hello_1(v) \
{hello_1 _hello_1_tchk, _ADT_v=(v); \
int t = hello_1_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_hello_1(hello_1 v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v&~LOW_BIT_USED));
	}
}

;
struct _ADT_hello_1_Cons_hello_1 {
    c_string f0;
    ADT_int (*(*f1)(ADT_int))(ADT_char);
    c_string f2;
    ADT_int (*f3)(ADT_char);};
#define if_Cons_hello_1(v, v0, v1, v2, v3) \
{hello_1 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0) {\
c_string v0=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f0; \
ADT_int (*(*v1)(ADT_int))(ADT_char)=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f1; \
c_string v2=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f2; \
ADT_int (*v3)(ADT_char)=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f3; 
#define else_if_Cons_hello_1(v0, v1, v2, v3) \
} else if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
c_string v0=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f0; \
ADT_int (*(*v1)(ADT_int))(ADT_char)=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f1; \
c_string v2=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f2; \
ADT_int (*v3)(ADT_char)=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f3; 
#define if_Cons_hello_1_ptr(v, v0, v1, v2, v3) \
{hello_1 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==0)  {\
c_string *v0=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f0; \
ADT_int (*(**v1)(ADT_int))(ADT_char)=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f1; \
c_string *v2=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f2; \
ADT_int (**v3)(ADT_char)=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f3; 
#define case_Cons_hello_1(v0, v1, v2, v3) \
break;}} case 1: {{hello_1 _SW_tchk=_hello_1_tchk;}{char _hello_1_tchk; \
c_string v0=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f0; \
ADT_int (*(*v1)(ADT_int))(ADT_char)=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f1; \
c_string v2=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f2; \
ADT_int (*v3)(ADT_char)=((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f3; 
#define case_Cons_hello_1_ptr(v0, v1, v2, v3) \
break;}} case 1: {{hello_1 _SW_tchk=_hello_1_tchk;}{char _hello_1_tchk; \
c_string *v0=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f0; \
ADT_int (*(**v1)(ADT_int))(ADT_char)=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f1; \
c_string *v2=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f2; \
ADT_int (**v3)(ADT_char)=&((struct _ADT_hello_1_Cons_hello_1*)((uintptr_t)_ADT_v-0))->f3; 

static __inline hello_1 Cons_hello_1(c_string v0, ADT_int (*(*v1)(ADT_int))(ADT_char), c_string v2, ADT_int (*v3)(ADT_char)){
	struct _ADT_hello_1_Cons_hello_1 *v=(struct _ADT_hello_1_Cons_hello_1*)ADT_MALLOC(sizeof(struct _ADT_hello_1_Cons_hello_1));
	v->f0=v0; 
	v->f1=v1; 
	v->f2=v2; 
	v->f3=v3; 
	return (hello_1)(0+(uintptr_t)v);
}
;
struct _ADT_hello_1_nil_hello_1 {};
#define if_nil_hello_1(v) \
{hello_1 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_hello_1() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_hello_1_ptr(v) \
{hello_1 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_hello_1() \
break;}} case 0: {{hello_1 _SW_tchk=_hello_1_tchk;}{char _hello_1_tchk; 
#define case_nil_hello_1_ptr() \
break;}} case 0: {{hello_1 _SW_tchk=_hello_1_tchk;}{char _hello_1_tchk; 

static __inline hello_1 nil_hello_1(){
	struct _ADT_hello_1_nil_hello_1 *v=(struct _ADT_hello_1_nil_hello_1*)0;
	return (hello_1)((uintptr_t)v);
}
;
struct _ADT_hello_1_Hi_hello_1 {
    ADT_char (*f0)(ADT_int);
    c_string f1;};
#define if_Hi_hello_1(v, v0, v1) \
{hello_1 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1) {\
ADT_char (*v0)(ADT_int)=((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f0; \
c_string v1=((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f1; 
#define else_if_Hi_hello_1(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
ADT_char (*v0)(ADT_int)=((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f0; \
c_string v1=((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f1; 
#define if_Hi_hello_1_ptr(v, v0, v1) \
{hello_1 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1 && ((uintptr_t)(_ADT_v)&LOW_BIT_USED)==1)  {\
ADT_char (**v0)(ADT_int)=&((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f0; \
c_string *v1=&((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f1; 
#define case_Hi_hello_1(v0, v1) \
break;}} case 2: {{hello_1 _SW_tchk=_hello_1_tchk;}{char _hello_1_tchk; \
ADT_char (*v0)(ADT_int)=((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f0; \
c_string v1=((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f1; 
#define case_Hi_hello_1_ptr(v0, v1) \
break;}} case 2: {{hello_1 _SW_tchk=_hello_1_tchk;}{char _hello_1_tchk; \
ADT_char (**v0)(ADT_int)=&((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f0; \
c_string *v1=&((struct _ADT_hello_1_Hi_hello_1*)((uintptr_t)_ADT_v-1))->f1; 

static __inline hello_1 Hi_hello_1(ADT_char (*v0)(ADT_int), c_string v1){
	struct _ADT_hello_1_Hi_hello_1 *v=(struct _ADT_hello_1_Hi_hello_1*)ADT_MALLOC(sizeof(struct _ADT_hello_1_Hi_hello_1));
	v->f0=v0; 
	v->f1=v1; 
	return (hello_1)(1+(uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/

ADT_int f(ADT_1);

/* Function Aliases */
/*----------------------------------------------------------------------------*/

static __inline ADT_int f_1(ADT_int v0){
    return (ADT_int) f((ADT_1) v0);
}

/*----------------------------------------------------------------------------*/
