#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_alison{} *alison;
#define alison_T_NUM_CONST (0) 
#define alison_T_NUM_NONCONST (1) 
#define alison_constructorNum(v) \
(( alison_T_NUM_NONCONST>0 && v >= alison_T_NUM_CONST ?  \
 ( alison_T_NUM_NONCONST==1 ?  alison_T_NUM_CONST   : \
(( alison_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + alison_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + alison_T_NUM_CONST)) : v ) )

static __inline void free_alison(alison v);
#define switch_alison(v) \
{alison _alison_tchk, _ADT_v=(v); \
int t = alison_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_alison(alison v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_alison_french {
    int (*)(int (*)(ADT_1)) f0;};
#define if_french(v, v0) \
{alison _ADT_v=(v);if (1) {\
int (*)(int (*)(ADT_1)) v0=((struct _ADT_alison_french*)((uintptr_t)_ADT_v-0))->f0; 
#define else_if_french(v0) \
} else if (1) {\
int (*)(int (*)(ADT_1)) v0=((struct _ADT_alison_french*)((uintptr_t)_ADT_v-0))->f0; 
#define if_french_ptr(v, v0) \
{alison _ADT_v=(v);if (1) {\
int (*)(int (*)(ADT_1)) *v0=&((struct _ADT_alison_french*)((uintptr_t)_ADT_v-0))->f0; 
#define case_french(v0) \
break;}} case 0: {{alison _SW_tchk=_alison_tchk;}{char _alison_tchk; \
int (*)(int (*)(ADT_1)) v0=((struct _ADT_alison_french*)((uintptr_t)_ADT_v-0))->f0; 
#define case_french_ptr(v0) \
break;}} case 0: {{alison _SW_tchk=_alison_tchk;}{char _alison_tchk; \
int (*)(int (*)(ADT_1)) *v0=&((struct _ADT_alison_french*)((uintptr_t)_ADT_v-0))->f0; 

static __inline alison french(int (*)(int (*)(ADT_1)) v0){
	struct _ADT_alison_french *v=(struct _ADT_alison_french*)ADT_MALLOC(sizeof(struct _ADT_alison_french));
	v->f0=v0; 
	return (alison)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_ali{} *ali;
#define ali_T_NUM_CONST (0) 
#define ali_T_NUM_NONCONST (1) 
#define ali_constructorNum(v) \
(( ali_T_NUM_NONCONST>0 && v >= ali_T_NUM_CONST ?  \
 ( ali_T_NUM_NONCONST==1 ?  ali_T_NUM_CONST   : \
(( ali_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + ali_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + ali_T_NUM_CONST)) : v ) )

static __inline void free_ali(ali v);
#define switch_ali(v) \
{ali _ali_tchk, _ADT_v=(v); \
int t = ali_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_ali(ali v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_ali_french_ali {
    int (*)(int (*)(c_string)) f0;};
#define if_french_ali(v, v0) \
{ali _ADT_v=(v);if (1) {\
int (*)(int (*)(c_string)) v0=((struct _ADT_ali_french_ali*)((uintptr_t)_ADT_v-0))->f0; 
#define else_if_french_ali(v0) \
} else if (1) {\
int (*)(int (*)(c_string)) v0=((struct _ADT_ali_french_ali*)((uintptr_t)_ADT_v-0))->f0; 
#define if_french_ali_ptr(v, v0) \
{ali _ADT_v=(v);if (1) {\
int (*)(int (*)(c_string)) *v0=&((struct _ADT_ali_french_ali*)((uintptr_t)_ADT_v-0))->f0; 
#define case_french_ali(v0) \
break;}} case 0: {{ali _SW_tchk=_ali_tchk;}{char _ali_tchk; \
int (*)(int (*)(c_string)) v0=((struct _ADT_ali_french_ali*)((uintptr_t)_ADT_v-0))->f0; 
#define case_french_ali_ptr(v0) \
break;}} case 0: {{ali _SW_tchk=_ali_tchk;}{char _ali_tchk; \
int (*)(int (*)(c_string)) *v0=&((struct _ADT_ali_french_ali*)((uintptr_t)_ADT_v-0))->f0; 

static __inline ali french_ali(int (*)(int (*)(c_string)) v0){
	struct _ADT_ali_french_ali *v=(struct _ADT_ali_french_ali*)ADT_MALLOC(sizeof(struct _ADT_ali_french_ali));
	v->f0=v0; 
	return (ali)(0+(uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/


/* Function Aliases */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
