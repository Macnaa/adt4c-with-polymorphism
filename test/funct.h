#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_list{} *list;
#define list_T_NUM_CONST (1) 
#define list_T_NUM_NONCONST (1) 
#define list_constructorNum(v) \
(( list_T_NUM_NONCONST>0 && v >= list_T_NUM_CONST ?  \
 ( list_T_NUM_NONCONST==1 ?  list_T_NUM_CONST   : \
(( list_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_T_NUM_CONST)) : v ) )

static __inline void free_list(list v);
#define switch_list(v) \
{list _list_tchk, _ADT_v=(v); \
int t = list_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list(list v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_cons {
    ADT_1 f0;
    struct _ADT_list* f1;};
#define if_cons(v, v0, v1) \
{list _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_ptr(v, v0, v1) \
{list _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 *v0=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* *v1=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons(v0, v1) \
break;}} case 1: {{list _SW_tchk=_list_tchk;}{char _list_tchk; \
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_ptr(v0, v1) \
break;}} case 1: {{list _SW_tchk=_list_tchk;}{char _list_tchk; \
ADT_1 *v0=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* *v1=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list cons(ADT_1 v0, struct _ADT_list* v1){
	struct _ADT_list_cons *v=(struct _ADT_list_cons*)ADT_MALLOC(sizeof(struct _ADT_list_cons));
	v->f0=v0; 
	v->f1=v1; 
	return (list)(0+(uintptr_t)v);
}
;
struct _ADT_list_nil {};
#define if_nil(v) \
{list _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_ptr(v) \
{list _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil() \
break;}} case 0: {{list _SW_tchk=_list_tchk;}{char _list_tchk; 
#define case_nil_ptr() \
break;}} case 0: {{list _SW_tchk=_list_tchk;}{char _list_tchk; 

static __inline list nil(){
	struct _ADT_list_nil *v=(struct _ADT_list_nil*)0;
	return (list)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_stringlist{} *stringlist;
#define stringlist_T_NUM_CONST (1) 
#define stringlist_T_NUM_NONCONST (1) 
#define stringlist_constructorNum(v) \
(( stringlist_T_NUM_NONCONST>0 && v >= stringlist_T_NUM_CONST ?  \
 ( stringlist_T_NUM_NONCONST==1 ?  stringlist_T_NUM_CONST   : \
(( stringlist_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + stringlist_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + stringlist_T_NUM_CONST)) : v ) )

static __inline void free_stringlist(stringlist v);
#define switch_stringlist(v) \
{stringlist _stringlist_tchk, _ADT_v=(v); \
int t = stringlist_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_stringlist(stringlist v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_stringlist_cons_stringlist {
    c_string f0;
    struct _ADT_stringlist* f1;};
#define if_cons_stringlist(v, v0, v1) \
{stringlist _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
c_string v0=((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist* v1=((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_stringlist(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
c_string v0=((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist* v1=((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_stringlist_ptr(v, v0, v1) \
{stringlist _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
c_string *v0=&((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist* *v1=&((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_stringlist(v0, v1) \
break;}} case 1: {{stringlist _SW_tchk=_stringlist_tchk;}{char _stringlist_tchk; \
c_string v0=((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist* v1=((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_stringlist_ptr(v0, v1) \
break;}} case 1: {{stringlist _SW_tchk=_stringlist_tchk;}{char _stringlist_tchk; \
c_string *v0=&((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_stringlist* *v1=&((struct _ADT_stringlist_cons_stringlist*)((uintptr_t)_ADT_v-0))->f1; 

static __inline stringlist cons_stringlist(c_string v0, struct _ADT_stringlist* v1){
	struct _ADT_stringlist_cons_stringlist *v=(struct _ADT_stringlist_cons_stringlist*)ADT_MALLOC(sizeof(struct _ADT_stringlist_cons_stringlist));
	v->f0=v0; 
	v->f1=v1; 
	return (stringlist)(0+(uintptr_t)v);
}
;
struct _ADT_stringlist_nil_stringlist {};
#define if_nil_stringlist(v) \
{stringlist _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_stringlist() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_stringlist_ptr(v) \
{stringlist _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_stringlist() \
break;}} case 0: {{stringlist _SW_tchk=_stringlist_tchk;}{char _stringlist_tchk; 
#define case_nil_stringlist_ptr() \
break;}} case 0: {{stringlist _SW_tchk=_stringlist_tchk;}{char _stringlist_tchk; 

static __inline stringlist nil_stringlist(){
	struct _ADT_stringlist_nil_stringlist *v=(struct _ADT_stringlist_nil_stringlist*)0;
	return (stringlist)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_list3{} *list3;
#define list3_T_NUM_CONST (1) 
#define list3_T_NUM_NONCONST (1) 
#define list3_constructorNum(v) \
(( list3_T_NUM_NONCONST>0 && v >= list3_T_NUM_CONST ?  \
 ( list3_T_NUM_NONCONST==1 ?  list3_T_NUM_CONST   : \
(( list3_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list3_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list3_T_NUM_CONST)) : v ) )

static __inline void free_list3(list3 v);
#define switch_list3(v) \
{list3 _list3_tchk, _ADT_v=(v); \
int t = list3_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list3(list3 v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list3_cons_list3 {
    ADT_3 f0;
    struct _ADT_list3* f1;};
#define if_cons_list3(v, v0, v1) \
{list3 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_3 v0=((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list3* v1=((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list3(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_3 v0=((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list3* v1=((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list3_ptr(v, v0, v1) \
{list3 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_3 *v0=&((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list3* *v1=&((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list3(v0, v1) \
break;}} case 1: {{list3 _SW_tchk=_list3_tchk;}{char _list3_tchk; \
ADT_3 v0=((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list3* v1=((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list3_ptr(v0, v1) \
break;}} case 1: {{list3 _SW_tchk=_list3_tchk;}{char _list3_tchk; \
ADT_3 *v0=&((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list3* *v1=&((struct _ADT_list3_cons_list3*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list3 cons_list3(ADT_3 v0, struct _ADT_list3* v1){
	struct _ADT_list3_cons_list3 *v=(struct _ADT_list3_cons_list3*)ADT_MALLOC(sizeof(struct _ADT_list3_cons_list3));
	v->f0=v0; 
	v->f1=v1; 
	return (list3)(0+(uintptr_t)v);
}
;
struct _ADT_list3_nil_list3 {};
#define if_nil_list3(v) \
{list3 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list3() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list3_ptr(v) \
{list3 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list3() \
break;}} case 0: {{list3 _SW_tchk=_list3_tchk;}{char _list3_tchk; 
#define case_nil_list3_ptr() \
break;}} case 0: {{list3 _SW_tchk=_list3_tchk;}{char _list3_tchk; 

static __inline list3 nil_list3(){
	struct _ADT_list3_nil_list3 *v=(struct _ADT_list3_nil_list3*)0;
	return (list3)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_intlist{} *intlist;
#define intlist_T_NUM_CONST (1) 
#define intlist_T_NUM_NONCONST (1) 
#define intlist_constructorNum(v) \
(( intlist_T_NUM_NONCONST>0 && v >= intlist_T_NUM_CONST ?  \
 ( intlist_T_NUM_NONCONST==1 ?  intlist_T_NUM_CONST   : \
(( intlist_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + intlist_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + intlist_T_NUM_CONST)) : v ) )

static __inline void free_intlist(intlist v);
#define switch_intlist(v) \
{intlist _intlist_tchk, _ADT_v=(v); \
int t = intlist_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_intlist(intlist v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_intlist_cons_intlist {
    int f0;
    struct _ADT_intlist* f1;};
#define if_cons_intlist(v, v0, v1) \
{intlist _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
int v0=((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_intlist* v1=((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_intlist(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
int v0=((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_intlist* v1=((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_intlist_ptr(v, v0, v1) \
{intlist _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
int *v0=&((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_intlist* *v1=&((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_intlist(v0, v1) \
break;}} case 1: {{intlist _SW_tchk=_intlist_tchk;}{char _intlist_tchk; \
int v0=((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_intlist* v1=((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_intlist_ptr(v0, v1) \
break;}} case 1: {{intlist _SW_tchk=_intlist_tchk;}{char _intlist_tchk; \
int *v0=&((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_intlist* *v1=&((struct _ADT_intlist_cons_intlist*)((uintptr_t)_ADT_v-0))->f1; 

static __inline intlist cons_intlist(int v0, struct _ADT_intlist* v1){
	struct _ADT_intlist_cons_intlist *v=(struct _ADT_intlist_cons_intlist*)ADT_MALLOC(sizeof(struct _ADT_intlist_cons_intlist));
	v->f0=v0; 
	v->f1=v1; 
	return (intlist)(0+(uintptr_t)v);
}
;
struct _ADT_intlist_nil_intlist {};
#define if_nil_intlist(v) \
{intlist _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_intlist() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_intlist_ptr(v) \
{intlist _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_intlist() \
break;}} case 0: {{intlist _SW_tchk=_intlist_tchk;}{char _intlist_tchk; 
#define case_nil_intlist_ptr() \
break;}} case 0: {{intlist _SW_tchk=_intlist_tchk;}{char _intlist_tchk; 

static __inline intlist nil_intlist(){
	struct _ADT_intlist_nil_intlist *v=(struct _ADT_intlist_nil_intlist*)0;
	return (intlist)((uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/

int (*(*(*(*hello(int (*(*(*(*)(int))(int))(int))(int)))(int))(int))(int))(int);

/* Function Aliases */
/*----------------------------------------------------------------------------*/

static __inline int (*(*(*(*ihello(int (*(*(*(*v0)(int))(int))(int))(int)))(int))(int))(int))(int){
    return (int (*(*(*(*)(int))(int))(int))(int)) hello((int (*(*(*(*)(int))(int))(int))(int)) v0);
}

/*----------------------------------------------------------------------------*/
