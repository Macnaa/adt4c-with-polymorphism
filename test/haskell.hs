
reverse_list :: [a] -> [a]
reverse_list [] = []
reverse_list (x:xs) = (reverse_list xs) ++ [x]

length_list :: [a] -> Int
length_list [] = 0
length_list (x:xs) = 1 + length_list xs

concat_lists :: [a] -> [a] -> [a]
concat_lists [] ys = ys
concat_lists (x:xs) ys = x : (concat_lists xs ys)