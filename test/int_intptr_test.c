#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

intptr_t f(intptr_t a){
	return a-1;
}

int g(int a){
	return a-1;
}

char h(intptr_t a){
	return a;
}

intptr_t i(char a){
	return a;
}

void main(){
	int a = 6;
	intptr_t b = 1000;
	char c = 'c';
	intptr_t d = 'd';

	printf("%i, %c\n", (int)d,(char)b);
}
