#include <stdio.h>

typedef struct ADT_gen_1 {} *ADT_1;

int f_0(char a){
    return 0;
}

int (*f_1(int a, int b))(char){
    return f_0;
}

int (*(*f_2(int a))(int,int))(char){
    printf("HI\n");
    return f_1; 
}

void main(){
    ADT_1 (*(*f(ADT_1))(ADT_1,ADT_1))(ADT_1);
        f = (ADT_1 (*(*(ADT_1 ))(ADT_1,ADT_1))(ADT_1)) f_2;
}

//long (*(*f(int ))(char ))(void )

//return3 (*  (*f(arg))(arg)  )(arg){..}

//return4 (* (* (*f(arg))(arg))(arg))(arg) {..}

/* return f (args)

if_arg(return, name)
    name f
    
else_if_argfunc(return, return_val, para_list)
    start <- getstart(return_val);
    end <- getend(para_list);
    
    start f_name(f_args) end {
end_if() */

