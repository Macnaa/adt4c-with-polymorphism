#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

void f_1(void (*)(int));

void f_2(){
    printf("F2\n");
}

void f_1(void (*a)(int)){
    printf("F1\n");
    a(7);
}

void f_0(void (*a)(void (*)(int))){
    printf("F0\n");
    a(*f_2);
}

void main(){
    f_0(*f_1);
}