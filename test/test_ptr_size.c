#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int f(int a){
	return 6;
}

void main(){
	intptr_t a = 5;
	uintptr_t b = 5;
	double c = 5.6;
	int (*vf)(int) =&f;
	struct {} g;
	float h = 89.0;

	printf("%i\n", (int)sizeof(a));
	printf("%i\n", (int)sizeof(b));
	printf("%i\n", (int)sizeof(c));
	printf("%i\n", (int)sizeof(vf));
	printf("%i\n", (int)sizeof(&g));
	printf("%i\n", (int)sizeof(h));
}

