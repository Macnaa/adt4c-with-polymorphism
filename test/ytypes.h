#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

Mono Type def h file


Mono Type def function


H File Generation


Function Generation


END


/***************************/
typedef struct _ADT_Type{} *Type;
#define Type_T_NUM_CONST (1) 
#define Type_T_NUM_NONCONST (1) 
#define Type_constructorNum(v) \
(( Type_T_NUM_NONCONST>0 && v >= Type_T_NUM_CONST ?  \
 ( Type_T_NUM_NONCONST==1 ?  Type_T_NUM_CONST   : \
(( Type_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + Type_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + Type_T_NUM_CONST)) : v ) )

static __inline void free_Type(Type v);
#define switch_Type(v) \
{Type _Type_tchk, _ADT_v=(v); \
int t = Type_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_Type(Type v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_Type_Type1 {
    int f0;
    char f1;};
#define if_Type1(v, v0, v1) \
{Type _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
int v0=((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f0; \
char v1=((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_Type1(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
int v0=((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f0; \
char v1=((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f1; 
#define if_Type1_ptr(v, v0, v1) \
{Type _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
int *v0=&((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f0; \
char *v1=&((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f1; 
#define case_Type1(v0, v1) \
break;}} case 1: {{Type _SW_tchk=_Type_tchk;}{char _Type_tchk; \
int v0=((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f0; \
char v1=((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f1; 
#define case_Type1_ptr(v0, v1) \
break;}} case 1: {{Type _SW_tchk=_Type_tchk;}{char _Type_tchk; \
int *v0=&((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f0; \
char *v1=&((struct _ADT_Type_Type1*)((uintptr_t)_ADT_v-0))->f1; 

static __inline Type Type1(int v0, char v1){
	struct _ADT_Type_Type1 *v=(struct _ADT_Type_Type1*)ADT_MALLOC(sizeof(struct _ADT_Type_Type1));
	v->f0=v0; 
	v->f1=v1; 
	return (Type)(0+(uintptr_t)v);
}

H File Generation


Function Generation


END

;
struct _ADT_Type_Type2 {};
#define if_Type2(v) \
{Type _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_Type2() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_Type2_ptr(v) \
{Type _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_Type2() \
break;}} case 0: {{Type _SW_tchk=_Type_tchk;}{char _Type_tchk; 
#define case_Type2_ptr() \
break;}} case 0: {{Type _SW_tchk=_Type_tchk;}{char _Type_tchk; 

static __inline Type Type2(){
	struct _ADT_Type_Type2 *v=(struct _ADT_Type_Type2*)0;
	return (Type)((uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/


/* Function Aliases */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
