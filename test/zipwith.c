#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include "zipwith.h"

c_string concat(c_string s_1, c_string s_2){
    c_string result = malloc(strlen(s_1) + strlen(s_2) + 1);
    
    strcpy(result, s_1);
    strcat(result, s_2);
    
    return result;
}

swapped swap_pair(pair_t p){ 
    swapped s_p; 

    if_pair(p, first, second)
        s_p = pair_swapped(second, first);
        return s_p;
    end_if()
}


list_gen3 zipWith(ADT_3 (*f)(ADT_1, ADT_2), list list1, list_gen2 list2){
    if_cons(list1, lhead, ltail)
        if_cons_list_gen2(list2, lhead2,ltail2)
            return cons_list_gen3((*f)(lhead,lhead2), zipWith(f, ltail, ltail2));
        else()
            printf("Error: Lists must be the same length\n");
        end_if();
    else()
        return nil_list_gen3();
    end_if()
}

void print_list_string(list_string s){
    if_cons_list_string(s, shead, stail)
        printf("%s", shead);
        print_list_string(stail);
    else()
        printf("\n");
    end_if()
}

void main(){
    list_string liststring1 = cons_list_string("Hello ", cons_list_string("I'm ", nil_list_string()));
    list_string liststring2 = cons_list_string("Friend ", cons_list_string("Aleck!", nil_list_string()));
    pair_list_string pls = pair_pair_list_string(liststring2, liststring1);
    

     list_string sentence;
    
 
    
        sentence = zipWith_string(*concat, liststring1, liststring2);

    
    print_list_string(sentence);

    exit(0);
}