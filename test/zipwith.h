#include <stdint.h>
#include <stdlib.h>
typedef char *c_string;
typedef char *ADT_string;
typedef intptr_t ADT_int;
typedef intptr_t ADT_char;
typedef uintptr_t ADT_uint;
#define end_switch() }}}}
#define default() break;}} default: {{
#define else() } else {
#define end_if() }}
#ifndef ADT_MALLOC
#define ADT_MALLOC(s) malloc(s)
#endif
#ifndef ADT_FREE
#define ADT_FREE(s) free(s)
#endif
#ifndef LOW_BIT_USED
#define LOW_BIT_USED 7
#endif

typedef struct _ADT_Gen_1{} *ADT_1;
typedef struct _ADT_Gen_2{} *ADT_2;
typedef struct _ADT_Gen_3{} *ADT_3;
typedef struct _ADT_Gen_4{} *ADT_4;
typedef struct _ADT_Gen_5{} *ADT_5;
typedef struct _ADT_Gen_6{} *ADT_6;
typedef struct _ADT_Gen_7{} *ADT_7;
typedef struct _ADT_Gen_8{} *ADT_8;
typedef struct _ADT_Gen_9{} *ADT_9;
typedef struct _ADT_Gen_10{} *ADT_10;
typedef struct _ADT_Gen_11{} *ADT_11;
typedef struct _ADT_Gen_12{} *ADT_12;
typedef struct _ADT_Gen_13{} *ADT_13;
typedef struct _ADT_Gen_14{} *ADT_14;
typedef struct _ADT_Gen_15{} *ADT_15;
typedef struct _ADT_Gen_16{} *ADT_16;
typedef struct _ADT_Gen_17{} *ADT_17;
typedef struct _ADT_Gen_18{} *ADT_18;
typedef struct _ADT_Gen_19{} *ADT_19;
typedef struct _ADT_Gen_20{} *ADT_20;

/***************************/
typedef struct _ADT_pair_t{} *pair_t;
#define pair_t_T_NUM_CONST (0) 
#define pair_t_T_NUM_NONCONST (1) 
#define pair_t_constructorNum(v) \
(( pair_t_T_NUM_NONCONST>0 && v >= pair_t_T_NUM_CONST ?  \
 ( pair_t_T_NUM_NONCONST==1 ?  pair_t_T_NUM_CONST   : \
(( pair_t_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + pair_t_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + pair_t_T_NUM_CONST)) : v ) )

static __inline void free_pair_t(pair_t v);
#define switch_pair_t(v) \
{pair_t _pair_t_tchk, _ADT_v=(v); \
int t = pair_t_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_pair_t(pair_t v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_pair_t_pair {
    ADT_1 f0;
    ADT_2 f1;};
#define if_pair(v, v0, v1) \
{pair_t _ADT_v=(v);if (1) {\
ADT_1 v0=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 v1=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair(v0, v1) \
} else if (1) {\
ADT_1 v0=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 v1=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_ptr(v, v0, v1) \
{pair_t _ADT_v=(v);if (1) {\
ADT_1 *v0=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 *v1=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair(v0, v1) \
break;}} case 0: {{pair_t _SW_tchk=_pair_t_tchk;}{char _pair_t_tchk; \
ADT_1 v0=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 v1=((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_ptr(v0, v1) \
break;}} case 0: {{pair_t _SW_tchk=_pair_t_tchk;}{char _pair_t_tchk; \
ADT_1 *v0=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f0; \
ADT_2 *v1=&((struct _ADT_pair_t_pair*)((uintptr_t)_ADT_v-0))->f1; 

static __inline pair_t pair(ADT_1 v0, ADT_2 v1){
	struct _ADT_pair_t_pair *v=(struct _ADT_pair_t_pair*)ADT_MALLOC(sizeof(struct _ADT_pair_t_pair));
	v->f0=v0; 
	v->f1=v1; 
	return (pair_t)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_list{} *list;
#define list_T_NUM_CONST (1) 
#define list_T_NUM_NONCONST (1) 
#define list_constructorNum(v) \
(( list_T_NUM_NONCONST>0 && v >= list_T_NUM_CONST ?  \
 ( list_T_NUM_NONCONST==1 ?  list_T_NUM_CONST   : \
(( list_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_T_NUM_CONST)) : v ) )

static __inline void free_list(list v);
#define switch_list(v) \
{list _list_tchk, _ADT_v=(v); \
int t = list_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list(list v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_cons {
    ADT_1 f0;
    struct _ADT_list* f1;};
#define if_cons(v, v0, v1) \
{list _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_ptr(v, v0, v1) \
{list _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_1 *v0=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* *v1=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons(v0, v1) \
break;}} case 1: {{list _SW_tchk=_list_tchk;}{char _list_tchk; \
ADT_1 v0=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* v1=((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_ptr(v0, v1) \
break;}} case 1: {{list _SW_tchk=_list_tchk;}{char _list_tchk; \
ADT_1 *v0=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list* *v1=&((struct _ADT_list_cons*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list cons(ADT_1 v0, struct _ADT_list* v1){
	struct _ADT_list_cons *v=(struct _ADT_list_cons*)ADT_MALLOC(sizeof(struct _ADT_list_cons));
	v->f0=v0; 
	v->f1=v1; 
	return (list)(0+(uintptr_t)v);
}
;
struct _ADT_list_nil {};
#define if_nil(v) \
{list _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_ptr(v) \
{list _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil() \
break;}} case 0: {{list _SW_tchk=_list_tchk;}{char _list_tchk; 
#define case_nil_ptr() \
break;}} case 0: {{list _SW_tchk=_list_tchk;}{char _list_tchk; 

static __inline list nil(){
	struct _ADT_list_nil *v=(struct _ADT_list_nil*)0;
	return (list)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_string{} *list_string;
#define list_string_T_NUM_CONST (1) 
#define list_string_T_NUM_NONCONST (1) 
#define list_string_constructorNum(v) \
(( list_string_T_NUM_NONCONST>0 && v >= list_string_T_NUM_CONST ?  \
 ( list_string_T_NUM_NONCONST==1 ?  list_string_T_NUM_CONST   : \
(( list_string_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_string_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_string_T_NUM_CONST)) : v ) )

static __inline void free_list_string(list_string v);
#define switch_list_string(v) \
{list_string _list_string_tchk, _ADT_v=(v); \
int t = list_string_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_string(list_string v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_string_cons_list_string {
    c_string f0;
    struct _ADT_list_string* f1;};
#define if_cons_list_string(v, v0, v1) \
{list_string _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
c_string v0=((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* v1=((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list_string(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
c_string v0=((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* v1=((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list_string_ptr(v, v0, v1) \
{list_string _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
c_string *v0=&((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* *v1=&((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_string(v0, v1) \
break;}} case 1: {{list_string _SW_tchk=_list_string_tchk;}{char _list_string_tchk; \
c_string v0=((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* v1=((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_string_ptr(v0, v1) \
break;}} case 1: {{list_string _SW_tchk=_list_string_tchk;}{char _list_string_tchk; \
c_string *v0=&((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* *v1=&((struct _ADT_list_string_cons_list_string*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_string cons_list_string(c_string v0, struct _ADT_list_string* v1){
	struct _ADT_list_string_cons_list_string *v=(struct _ADT_list_string_cons_list_string*)ADT_MALLOC(sizeof(struct _ADT_list_string_cons_list_string));
	v->f0=v0; 
	v->f1=v1; 
	return (list_string)(0+(uintptr_t)v);
}
;
struct _ADT_list_string_nil_list_string {};
#define if_nil_list_string(v) \
{list_string _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list_string() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list_string_ptr(v) \
{list_string _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list_string() \
break;}} case 0: {{list_string _SW_tchk=_list_string_tchk;}{char _list_string_tchk; 
#define case_nil_list_string_ptr() \
break;}} case 0: {{list_string _SW_tchk=_list_string_tchk;}{char _list_string_tchk; 

static __inline list_string nil_list_string(){
	struct _ADT_list_string_nil_list_string *v=(struct _ADT_list_string_nil_list_string*)0;
	return (list_string)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_gen2{} *list_gen2;
#define list_gen2_T_NUM_CONST (1) 
#define list_gen2_T_NUM_NONCONST (1) 
#define list_gen2_constructorNum(v) \
(( list_gen2_T_NUM_NONCONST>0 && v >= list_gen2_T_NUM_CONST ?  \
 ( list_gen2_T_NUM_NONCONST==1 ?  list_gen2_T_NUM_CONST   : \
(( list_gen2_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_gen2_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_gen2_T_NUM_CONST)) : v ) )

static __inline void free_list_gen2(list_gen2 v);
#define switch_list_gen2(v) \
{list_gen2 _list_gen2_tchk, _ADT_v=(v); \
int t = list_gen2_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_gen2(list_gen2 v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_gen2_cons_list_gen2 {
    ADT_2 f0;
    struct _ADT_list_gen2* f1;};
#define if_cons_list_gen2(v, v0, v1) \
{list_gen2 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_2 v0=((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen2* v1=((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list_gen2(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_2 v0=((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen2* v1=((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list_gen2_ptr(v, v0, v1) \
{list_gen2 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_2 *v0=&((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen2* *v1=&((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_gen2(v0, v1) \
break;}} case 1: {{list_gen2 _SW_tchk=_list_gen2_tchk;}{char _list_gen2_tchk; \
ADT_2 v0=((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen2* v1=((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_gen2_ptr(v0, v1) \
break;}} case 1: {{list_gen2 _SW_tchk=_list_gen2_tchk;}{char _list_gen2_tchk; \
ADT_2 *v0=&((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen2* *v1=&((struct _ADT_list_gen2_cons_list_gen2*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_gen2 cons_list_gen2(ADT_2 v0, struct _ADT_list_gen2* v1){
	struct _ADT_list_gen2_cons_list_gen2 *v=(struct _ADT_list_gen2_cons_list_gen2*)ADT_MALLOC(sizeof(struct _ADT_list_gen2_cons_list_gen2));
	v->f0=v0; 
	v->f1=v1; 
	return (list_gen2)(0+(uintptr_t)v);
}
;
struct _ADT_list_gen2_nil_list_gen2 {};
#define if_nil_list_gen2(v) \
{list_gen2 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list_gen2() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list_gen2_ptr(v) \
{list_gen2 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list_gen2() \
break;}} case 0: {{list_gen2 _SW_tchk=_list_gen2_tchk;}{char _list_gen2_tchk; 
#define case_nil_list_gen2_ptr() \
break;}} case 0: {{list_gen2 _SW_tchk=_list_gen2_tchk;}{char _list_gen2_tchk; 

static __inline list_gen2 nil_list_gen2(){
	struct _ADT_list_gen2_nil_list_gen2 *v=(struct _ADT_list_gen2_nil_list_gen2*)0;
	return (list_gen2)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_list_gen3{} *list_gen3;
#define list_gen3_T_NUM_CONST (1) 
#define list_gen3_T_NUM_NONCONST (1) 
#define list_gen3_constructorNum(v) \
(( list_gen3_T_NUM_NONCONST>0 && v >= list_gen3_T_NUM_CONST ?  \
 ( list_gen3_T_NUM_NONCONST==1 ?  list_gen3_T_NUM_CONST   : \
(( list_gen3_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + list_gen3_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + list_gen3_T_NUM_CONST)) : v ) )

static __inline void free_list_gen3(list_gen3 v);
#define switch_list_gen3(v) \
{list_gen3 _list_gen3_tchk, _ADT_v=(v); \
int t = list_gen3_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_list_gen3(list_gen3 v){
	if ((uintptr_t)(v) >= 1){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_list_gen3_cons_list_gen3 {
    ADT_3 f0;
    struct _ADT_list_gen3* f1;};
#define if_cons_list_gen3(v, v0, v1) \
{list_gen3 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_3 v0=((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen3* v1=((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_cons_list_gen3(v0, v1) \
} else if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_3 v0=((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen3* v1=((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f1; 
#define if_cons_list_gen3_ptr(v, v0, v1) \
{list_gen3 _ADT_v=(v);if ((uintptr_t)(_ADT_v) >= 1) {\
ADT_3 *v0=&((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen3* *v1=&((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_gen3(v0, v1) \
break;}} case 1: {{list_gen3 _SW_tchk=_list_gen3_tchk;}{char _list_gen3_tchk; \
ADT_3 v0=((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen3* v1=((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f1; 
#define case_cons_list_gen3_ptr(v0, v1) \
break;}} case 1: {{list_gen3 _SW_tchk=_list_gen3_tchk;}{char _list_gen3_tchk; \
ADT_3 *v0=&((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_gen3* *v1=&((struct _ADT_list_gen3_cons_list_gen3*)((uintptr_t)_ADT_v-0))->f1; 

static __inline list_gen3 cons_list_gen3(ADT_3 v0, struct _ADT_list_gen3* v1){
	struct _ADT_list_gen3_cons_list_gen3 *v=(struct _ADT_list_gen3_cons_list_gen3*)ADT_MALLOC(sizeof(struct _ADT_list_gen3_cons_list_gen3));
	v->f0=v0; 
	v->f1=v1; 
	return (list_gen3)(0+(uintptr_t)v);
}
;
struct _ADT_list_gen3_nil_list_gen3 {};
#define if_nil_list_gen3(v) \
{list_gen3 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define else_if_nil_list_gen3() \
} else if (((uintptr_t)(_ADT_v))==0) {
#define if_nil_list_gen3_ptr(v) \
{list_gen3 _ADT_v=(v);if (((uintptr_t)(_ADT_v))==0) {
#define case_nil_list_gen3() \
break;}} case 0: {{list_gen3 _SW_tchk=_list_gen3_tchk;}{char _list_gen3_tchk; 
#define case_nil_list_gen3_ptr() \
break;}} case 0: {{list_gen3 _SW_tchk=_list_gen3_tchk;}{char _list_gen3_tchk; 

static __inline list_gen3 nil_list_gen3(){
	struct _ADT_list_gen3_nil_list_gen3 *v=(struct _ADT_list_gen3_nil_list_gen3*)0;
	return (list_gen3)((uintptr_t)v);
}

/***************************/
typedef struct _ADT_swapped{} *swapped;
#define swapped_T_NUM_CONST (0) 
#define swapped_T_NUM_NONCONST (1) 
#define swapped_constructorNum(v) \
(( swapped_T_NUM_NONCONST>0 && v >= swapped_T_NUM_CONST ?  \
 ( swapped_T_NUM_NONCONST==1 ?  swapped_T_NUM_CONST   : \
(( swapped_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + swapped_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + swapped_T_NUM_CONST)) : v ) )

static __inline void free_swapped(swapped v);
#define switch_swapped(v) \
{swapped _swapped_tchk, _ADT_v=(v); \
int t = swapped_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_swapped(swapped v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_swapped_pair_swapped {
    ADT_2 f0;
    ADT_1 f1;};
#define if_pair_swapped(v, v0, v1) \
{swapped _ADT_v=(v);if (1) {\
ADT_2 v0=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 v1=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair_swapped(v0, v1) \
} else if (1) {\
ADT_2 v0=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 v1=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_swapped_ptr(v, v0, v1) \
{swapped _ADT_v=(v);if (1) {\
ADT_2 *v0=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 *v1=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_swapped(v0, v1) \
break;}} case 0: {{swapped _SW_tchk=_swapped_tchk;}{char _swapped_tchk; \
ADT_2 v0=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 v1=((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_swapped_ptr(v0, v1) \
break;}} case 0: {{swapped _SW_tchk=_swapped_tchk;}{char _swapped_tchk; \
ADT_2 *v0=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f0; \
ADT_1 *v1=&((struct _ADT_swapped_pair_swapped*)((uintptr_t)_ADT_v-0))->f1; 

static __inline swapped pair_swapped(ADT_2 v0, ADT_1 v1){
	struct _ADT_swapped_pair_swapped *v=(struct _ADT_swapped_pair_swapped*)ADT_MALLOC(sizeof(struct _ADT_swapped_pair_swapped));
	v->f0=v0; 
	v->f1=v1; 
	return (swapped)(0+(uintptr_t)v);
}

/***************************/
typedef struct _ADT_pair_list_string{} *pair_list_string;
#define pair_list_string_T_NUM_CONST (0) 
#define pair_list_string_T_NUM_NONCONST (1) 
#define pair_list_string_constructorNum(v) \
(( pair_list_string_T_NUM_NONCONST>0 && v >= pair_list_string_T_NUM_CONST ?  \
 ( pair_list_string_T_NUM_NONCONST==1 ?  pair_list_string_T_NUM_CONST   : \
(( pair_list_string_T_NUM_NONCONST > (LOW_BIT_USED + 1))  && ((v&LOW_BIT_USED) == (LOW_BIT_USED))  )  ?   \
 (*(int*)((uintptr_t)_ADT_v-LOW_BIT_USED)) + pair_list_string_T_NUM_CONST  : \
((v&(LOW_BIT_USED)) + pair_list_string_T_NUM_CONST)) : v ) )

static __inline void free_pair_list_string(pair_list_string v);
#define switch_pair_list_string(v) \
{pair_list_string _pair_list_string_tchk, _ADT_v=(v); \
int t = pair_list_string_constructorNum((uintptr_t)(_ADT_v)); \
switch(t) {{{


/******************************************************************************/
static __inline void free_pair_list_string(pair_list_string v){
	if ((uintptr_t)(v) >= 0){
		ADT_FREE((void*)((uintptr_t)v));
	}
}

;
struct _ADT_pair_list_string_pair_pair_list_string {
    struct _ADT_list_string* f0;
    struct _ADT_list_string* f1;};
#define if_pair_pair_list_string(v, v0, v1) \
{pair_list_string _ADT_v=(v);if (1) {\
struct _ADT_list_string* v0=((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* v1=((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define else_if_pair_pair_list_string(v0, v1) \
} else if (1) {\
struct _ADT_list_string* v0=((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* v1=((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define if_pair_pair_list_string_ptr(v, v0, v1) \
{pair_list_string _ADT_v=(v);if (1) {\
struct _ADT_list_string* *v0=&((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* *v1=&((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_pair_list_string(v0, v1) \
break;}} case 0: {{pair_list_string _SW_tchk=_pair_list_string_tchk;}{char _pair_list_string_tchk; \
struct _ADT_list_string* v0=((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* v1=((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f1; 
#define case_pair_pair_list_string_ptr(v0, v1) \
break;}} case 0: {{pair_list_string _SW_tchk=_pair_list_string_tchk;}{char _pair_list_string_tchk; \
struct _ADT_list_string* *v0=&((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f0; \
struct _ADT_list_string* *v1=&((struct _ADT_pair_list_string_pair_pair_list_string*)((uintptr_t)_ADT_v-0))->f1; 

static __inline pair_list_string pair_pair_list_string(struct _ADT_list_string* v0, struct _ADT_list_string* v1){
	struct _ADT_pair_list_string_pair_pair_list_string *v=(struct _ADT_pair_list_string_pair_pair_list_string*)ADT_MALLOC(sizeof(struct _ADT_pair_list_string_pair_pair_list_string));
	v->f0=v0; 
	v->f1=v1; 
	return (pair_list_string)(0+(uintptr_t)v);
}

/* Function Prototypes */
/*----------------------------------------------------------------------------*/

int hello(int, char, c_string);
swapped swap_pair(pair_t);
list_gen3 zipWith(ADT_3 (*)(ADT_1, ADT_2), list, list_gen2);

/* Function Aliases */
/*----------------------------------------------------------------------------*/

static __inline pair_list_string swap_list_string(pair_list_string v0){
    return (pair_list_string) swap_pair((pair_t) v0);
}

static __inline list_string zipWith_string(c_string (*v0)(c_string, c_string), list_string v1, list_string v2){
    return (list_string) zipWith((ADT_3 (*)(ADT_1, ADT_2)) v0, (list) v1, (list_gen2) v2);
}

/*----------------------------------------------------------------------------*/
